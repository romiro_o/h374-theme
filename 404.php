<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package H374
 * @since H374 1.0
 */

get_header();
?>
<section>
	<div class="container">
		<h1 class="page-title">Hier ist nichts</h1>
		<div class="error-404 not-found default-max-width">
			<div class="page-content">
				<p>Es sieht so aus, als ob an diesem Ort nichts gefunden wurde. Vielleicht eine Suche versuchen?</p>
				<div class="header__search" style="margin:20px 0;">
					<?php get_search_form(); ?>
				</div>
				
			</div><!-- .page-content -->
		</div><!-- .error-404 -->
	</div><!-- .container -->
</section>
<?php
get_footer();
