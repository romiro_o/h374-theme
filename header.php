<!DOCTYPE html>
<html lang="en">
<head>
	<?= get_theme_mod('head_scripts', '')?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--Favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="<?=get_template_directory_uri();?>/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?=get_template_directory_uri();?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?=get_template_directory_uri();?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?=get_template_directory_uri();?>/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?=get_template_directory_uri();?>/favicon/safari-pinned-tab.svg" color="#343230">
	<meta name="msapplication-TileColor" content="#343230">
	<meta name="theme-color" content="#343230">
    <!-- /END Favicon -->

    <?php wp_head();?>

</head>
<!-- Sprites svg -->
<?php include ('inc/sprites.php')?>
<body>
	<?= get_theme_mod('body_scripts', '')?>
<header>
	<div class="container">
		<div class="header__inner">
			<div class="header__logo">
				<a class="logo__link" href="/"><img src="<?= get_theme_mod('img_logo', '')?>" /></a>
			</div>
			<div class="header__top">
				<div class="header__contact">
					<div class="header__social">
						<a target="_blank" href="<?= get_theme_mod('youtube_link', '')?>"><svg class="social__icon"><use xlink:href="#ico-youtube"></use></svg></a>
						<a  target="_blank" href="<?= get_theme_mod('instagram_link', '')?>"><svg class="social__icon"><use xlink:href="#ico-instagram"></use></svg></a>
						<a  target="_blank" href="<?= get_theme_mod('facebook_link', '')?>"><svg class="social__icon"><use xlink:href="#ico-facebook"></use></svg></a>
					</div>
					<span><svg class="social__icon"><use xlink:href="#ico-watch"></use></svg><?= get_theme_mod('office_hours', '')?></span>
					<a href=""><svg class="social__icon"><use xlink:href="#ico-placemark"></use></svg><?= get_theme_mod('address', '')?></a>
					<a href="tel:+49<?=str_replace(array(' ', '(' , ')', '-'), '', get_theme_mod('phone_number', ''))?>"><svg class="social__icon"><use xlink:href="#ico-phone"></use></svg><?= get_theme_mod('phone_number', '')?></a>
				</div>
				<div class="header_menu">
					<nav class="header__nav">
						<div class="ico-mobil-menu">
							<div>
								<div class="bar1"></div>
								<div class="bar2"></div>
								<div class="bar3"></div>
							</div>
						</div>
						<div class="top__menu">
							<?php //wp_nav_menu(['theme_location' => 'top', 'container' => 'ul']); ?>
							<?php wp_nav_menu([
								'theme_location' => 'top',
								'container' => 'div',
								'container_id' => 'cssmenu']); ?>
                            <ul>
								<li class="mobile-menu-info">
									<svg class="social__icon"><use xlink:href="#ico-watch"></use></svg><span><?= get_theme_mod('office_hours', '')?></span>
								</li>
								<li class="mobile-menu-info">
									<div class="header__social">
										<a target="_blank" href="<?= get_theme_mod('youtube_link', '')?>"><svg class="social__icon"><use xlink:href="#ico-youtube"></use></svg></a>
										<a  target="_blank" href="<?= get_theme_mod('instagram_link', '')?>"><svg class="social__icon"><use xlink:href="#ico-instagram"></use></svg></a>
										<a  target="_blank" href="<?= get_theme_mod('facebook_link', '')?>"><svg class="social__icon"><use xlink:href="#ico-facebook"></use></svg></a>
									</div>
								</li>
							</ul>
						</div>
					</nav>
					<div class="header__mobile__logo">
						<a class="logo__link" href="/"><img src="<?= get_theme_mod('img_logo', '')?>" /></a>
					</div>
					<div class="header__mobile__phone">
						<a href="tel:+4904049200598"><svg class="social__icon"><use xlink:href="#ico-broken-phone"></use></svg></a>
					</div>
					<div class="header__search">
						<?php get_search_form(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>