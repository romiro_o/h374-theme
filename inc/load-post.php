<?php

$content_post = [
    1 => '<ul><li>Haarentfernung Laser</li><li>Haarentfernung SHR</li></ul>',
    2 => '<ul>
							<li>Fruchtsäure Peeling</li>
							<li>Carbon Peeling</li>
							<li>Aquafacial</li>
							<li>Chemische Peeling</li>
							<li>Hollywood Peeling</li>
							<li>Microdermabrasion</li>
							<li>PRX-t</li>
						</ul>',
	3 => '<ul>
							<li>Kryolipolyse</li>
							<li>Cellulite Behandlung</li>
							<li>Lymphdrainage</li>
							<li>Hydradermabrasion</li>
						</ul>',
	4 => '<ul>
							<li>Hyaluron Uterspritzung</li>
							<li>Microneedling</li>
							<li>Mesotherapie</li>
						</ul>',
	5 => '<ul>
							<li>Wimpernlifting</li>
							<li>Wimpernverlängerung</li>

						</ul>',
	6 => '<ul>
							<li>Zahnaufhellung für Weinliebhaber und Raucher</li>
							<li>Zahnaufhellung: Auffrischung Express</li>
						</ul>',


];

$images_post = [
        1 => 'Haarentfernung.png',
	2 => 'Peelings.png',
	3 => 'Körperpflege.png',
	4 => 'Antiaging.png',
    5 => 'Wimpernlifting.png',
	6 => 'Zahnbleaching2.png',
];

if (isset($_REQUEST['id']))
{
	$post_id = $_REQUEST['id'];
	echo json_encode([$content_post[$post_id],$images_post[$post_id]]);
	exit;
}
echo false;
exit;
/*
// в functions.php
add_action('wp_ajax_get_post_by_id', 'data_fetch');
add_action('wp_ajax_nopriv_get_post_by_id', 'data_fetch');

function data_fetch($postID){
    $postID = intval( $_POST['param'] );
    $args = array(
    'p' => $postID, // ID of a page, post, or custom type
    //'post_type' => 'any'
    );
    $recent = new WP_Query($args);
    while ( $recent->have_posts() ) : $recent->the_post();
        $link = '';
		get_the_post_thumbnail_url();
		the_content();
    endwhile;
    die();
}
*/
