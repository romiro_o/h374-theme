<?php
/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );

/**
 *
 */
function h374__style_frontend() {
	wp_enqueue_style('slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
	wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500&family=Playfair+Display:wght@400;500&display=swap');
	wp_enqueue_style('theme_style', get_stylesheet_directory_uri() . '/style.css');
}
add_action('wp_enqueue_scripts', 'h374__style_frontend');

add_action( 'wp_enqueue_scripts', 'h374__style_inner_pages', 25 );
function h374__style_inner_pages() {
	if( !is_home( ) ) {
		wp_enqueue_style('inner_page_style', get_stylesheet_directory_uri() . '/css/page-style.css');
	}
}

/**
 *
 */
function h374__include_theme_script(){
	wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.6.0.min.js', '', '1.0', false);
	wp_enqueue_script('slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', '', '1.0', false);
    wp_enqueue_script('theme_script', get_stylesheet_directory_uri() . '/js/script.js', '', '1.0', true);
}
add_action('wp_enqueue_scripts', 'h374__include_theme_script');

// Add support for Post Thumbnails.
add_theme_support( 'post-thumbnails' );
//set_post_thumbnail_size( 200, 150, true ); // Normal post thumbnails

// Add default posts and comments RSS feed links to head.
add_theme_support( 'automatic-feed-links' );

// Add custom menu support.
add_theme_support( 'menus' );
register_nav_menus(array(
	'top'  => 'Header menu',
	'bottom' => 'Footer menu'
));

/**
 * @param $atts
 * @param $item
 * @param $args
 * @return mixed
 */
function add_specific_menu_location_atts($atts, $item, $args ) {
	// check if the item is in the primary menu
	if( $args->theme_location == 'top' ||  $args->theme_location == 'bottom') {
		// add the desired attributes:
		$atts['class'] = 'nav__link';
	}
	return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_specific_menu_location_atts', 10, 3 );


add_action( 'widgets_init', 'register_my_widgets' );
/**
 *
 */
function register_my_widgets(){
$i = 0;
	register_sidebar( array(
		'name'          => sprintf(__('Sidebar %d'), $i ),
		'id'            => "sidebar-$i",
		'description'   => '',
		'class'         => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => "</li>\n",
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => "</h2>\n",
		'before_sidebar' => '', // WP 5.6
		'after_sidebar'  => '', // WP 5.6
	) );
}

/**
 * Plugin Name: CF7 Modal Invalid Answer
 * Plugin URI: https://gist.github.com/campusboy87/a056c288c99feee70058ed24cee805ad
 * Author: Campusboy (wp-plus)
 * Author URI: https://www.youtube.com/wp-plus
 */

add_action( 'wp_enqueue_scripts', 'wpcf7_modal_invalid_js' );
add_action( 'wp_footer', 'wpcf7_modal_invalid_js_inline', 999 );

/**
 * Поключает библиотеку sweetalert.js для создания красивых модальных окон.
 *
 * @link https://sweetalert.js.org/
 *
 * @return void
 */
function wpcf7_modal_invalid_js() {
	wp_enqueue_script( 'sweetalert', 'https://unpkg.com/sweetalert/dist/sweetalert.min.js' );
}
/**
 * Выводит на экран модальное окно при ошибках валидации формы.
 *
 * @return void
 */
function wpcf7_modal_invalid_js_inline() {
	?>
	<script>
        // Срабатывает при ошибках валидации формы.
        document.addEventListener('wpcf7invalid', function (response) {
            // Запускает модальное окно.
            swal({
                title: "Irrtum!",
                text: response.detail.apiResponse.message,
                icon: "error",
                button: "Schließen"
            });
        }, false);
	</script>

	<style>
		.wpcf7-validation-errors, .wpcf7-response-output {
			display: none !important;
		}
		.swal-button {
			padding: 12px 20px;
			font-size: 16px;
			border-radius: 0;
			background-color: #343230;
			border: 0;
		}
		.swal-button:not([disabled]):hover {
			background-color: #343230;
		}
		.swal-button:active {
			background-color: #343230;
		}
	</style>
	<?php
}
/**
 * Plugin Name: CF7 Modal Right Answer
 * Plugin URI: https://gist.github.com/campusboy87/a056c288c99feee70058ed24cee805ad
 * Author: Campusboy (wp-plus)
 * Author URI: https://www.youtube.com/wp-plus
 */

add_action( 'wp_enqueue_scripts', 'wpcf7_modal_mailsent_js' );
add_action( 'wp_footer', 'wpcf7_modal_mailsent_js_inline', 999 );

/**
 * Поключает библиотеку sweetalert.js для создания красивых модальных окон.
 *
 * @link https://sweetalert.js.org/
 *
 * @return void
 */
function wpcf7_modal_mailsent_js() {
	wp_enqueue_script( 'sweetalert', 'https://unpkg.com/sweetalert/dist/sweetalert.min.js' );
}

/**
 * Выводит на экран модальное окно при успешной отправки формы.
 *
 * @return void
 */
function wpcf7_modal_mailsent_js_inline() {
	?>
	<script>
        // Срабатывает при успешной отправке формы.
        document.addEventListener('wpcf7mailsent', function (response) {
            // Запускает модальное окно.
            swal({
                title: "Danke!",
                text: response.detail.apiResponse.message,
                icon: "success",
                button: "Schließen"
            });
        }, false);
	</script>

	<style>
		.wpcf7-mail-sent-ok, .wpcf7-response-output {
			display: none !important;
		}
		.swal-modal{
			font-weight: 400;
			line-height: 1.3;
			font-size: 16px;
		}
		.swal-title{
			font-size: 25px;
		}
		.swal-button {
			padding: 12px 20px;
			font-size: 16px;
			border-radius: 0;
			background-color: #343230;
			border: 0;
		}
		.swal-button:not([disabled]):hover {
			background-color: #343230;
		}
		.swal-button:active {
			background-color: #343230;
		}
	</style>
	<?php
}
/**
 * Add section address into theme customizer
 */
add_action(/**
 * @param $customizer
 */
	'customize_register', function($customizer){
	$customizer->add_section(
		'section_address',
		array(
			'title' => 'H374 - Contacts',
			'description' => 'Contacts - header & footer sections',
			'priority' => 35,
		)
	);
	$customizer->add_setting(
		'address',
		array(
			'default' => 'Our address',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'address',
		array(
			'label' => 'Our address (html is acceptable)',
			'section' => 'section_address',
			'type' => 'text',
		)
	);

	$customizer->add_setting(
		'office_hours',
		array(
			'default' => 'Mo. bis Fr.: 9 - 20 Uhr',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'office_hours',
		array(
			'label' => 'Office hours',
			'section' => 'section_address',
			'type' => 'text',
		)
	);


	$customizer->add_setting(
		'phone_number',
		array(
			'default' => '(040) 49 200 598',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'phone_number',
		array(
			'label' => 'Office phone',
			'section' => 'section_address',
			'type' => 'text',
		)
	);

	$customizer->add_setting(
		'youtube_link',
		array(
			'default' => '#',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'youtube_link',
		array(
			'label' => 'Youtube link (https://****)',
			'section' => 'section_address',
			'type' => 'text',
		)
	);

	$customizer->add_setting(
		'instagram_link',
		array(
			'default' => '#',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'instagram_link',
		array(
			'label' => 'Instagram link (https://****)',
			'section' => 'section_address',
			'type' => 'text',
		)
	);
	$customizer->add_setting(
		'facebook_link',
		array(
			'default' => '#',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'facebook_link',
		array(
			'label' => 'Facebook link (https://****)',
			'section' => 'section_address',
			'type' => 'text',
		)
	);
		
	$customizer->add_setting('img_logo');
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'img_logo',
			array(
				'label' => 'Header & Footer logo (svg/png/jpg)',
				'section' => 'section_address',
				'settings' => 'img_logo'
			)
		)
	);
	$customizer->add_setting(
		'path_policy',
		array(
			'default' => 'Set Path to Privacy Policy',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'path_policy',
		array(
			'label' => 'Path to Privacy Policy',
			'section' => 'section_address',
			'type' => 'text',
		)
	);
		
		//Seo section
	$customizer->add_section(
		'seo_section',
		array(
			'title' => 'H374 - Seo',
			'description' => 'Seo(keywords, descriptions)',
			'priority' => 36,
		)
	);

	$customizer->add_setting(
		'keywords',
		array(
			'default' => '',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'keywords',
		array(
			'label' => 'Default Keywords',
			'section' => 'seo_section',
			'type' => 'text',
		)
	);

	$customizer->add_setting(
		'description',
		array(
			'default' => '',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'description',
		array(
			'label' => 'Default Description',
			'section' => 'seo_section',
			'type' => 'text',
		)
	);

});

add_action(/**
 * @param $customizer
 */ 'customize_register', function($customizer){
	/**
	 * Позволяет использовать контрол textarea на странице настройки темы
	 */
	class Customize_Textarea_Control extends WP_Customize_Control {
		public $type = 'textarea';

		public function render_content() {
			?>
            <label>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
                <textarea rows="5" style="width:100%;" <?php $this->link(); ?>>
                    <?php echo esc_textarea($this->value()); ?>
                </textarea>
            </label>
			<?php
		}
	}

	$customizer->add_section(
		'section_scripts',
		array(
			'title' => 'H374 - Analytics & other scripts',
			'description' => 'Analytics(scripts into head & body sections)',
			'priority' => 35,
		)
	);

	$customizer->add_setting('head_scripts');
	$customizer->add_control(
		new Customize_Textarea_Control(
			$customizer,
			'head_scripts',
			array(
				'label' => 'Add scripts into <head> section',
				'section' => 'section_scripts',
				'type' => 'textarea'
			)
		)
	);
	$customizer->add_setting('body_scripts');
	$customizer->add_control(
		new Customize_Textarea_Control(
			$customizer,
			'body_scripts',
			array(
				'label' => 'Add scripts after <body>',
				'section' => 'section_scripts',
				'type' => 'textarea'
			)
		)
	);
	$customizer->add_setting('footer_scripts');
	$customizer->add_control(
		new Customize_Textarea_Control(
			$customizer,
			'footer_scripts',
			array(
				'label' => 'Add scripts before </body>',
				'section' => 'section_scripts',
				'type' => 'textarea'
			)
		)
	);

});

/**
 * check_sanitize_text()
 * Фильтрация данных из текстового поля
 * @param mixed $input
 * @return
 */
function check_sanitize_text($input) {
    return wp_kses_post(force_balance_tags($input));
}

add_action('wp_ajax_get_post_by_id', 'data_fetch');
add_action('wp_ajax_nopriv_get_post_by_id', 'data_fetch');
/**
 * @param $postID
 */
function data_fetch($postID){
    $postID = intval( $_POST['param'] );
    $args = array(
    	'p' => $postID, // ID of a page, post, or custom type
	);
    $recent = new WP_Query($args);
    while ( $recent->have_posts() ) : $recent->the_post();
        $link = '';
		$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		/*ob_start();
		the_content();
		$content = ob_get_contents(); // переменная $content теперь содержит контент записи
		ob_end_clean();*/
		$content = '';
		wp_send_json_success([$large_image_url[0], $content]);
		wp_die();
    endwhile;
    die();
}

add_action('wp_ajax_get_posts_by_category_id', 'data_fetch_posts');
add_action('wp_ajax_nopriv_get_posts_by_category_id', 'data_fetch_posts');
/**
 * Load category image by ID
 * @param $catID
 */
function data_fetch_posts($catID){
	$catID = intval( $_POST['param'] );
	$content = '';
	if($catID){
		$category = get_category($catID);
        if($category){
			$image = get_field('image', 'category' . '_' . $category->term_id);
			$size = 'large'; // (thumbnail, medium, large or custom size)
			$img_link = $image['sizes'][ $size ];
			wp_send_json_success([$img_link, $content]);
        }
        wp_die();
    }
	die();
}



add_action( 'wp_ajax_loadmore', 'true_loadmore' );
add_action( 'wp_ajax_nopriv_loadmore', 'true_loadmore' );
/**
 *
 */
function true_loadmore() {
    $paged = !empty( $_POST[ 'paged' ] ) ? $_POST[ 'paged' ] : 1;
	$category = !empty( $_POST[ 'category' ] ) ? $_POST[ 'category' ] : 1;
	$taxonomy = !empty( $_POST[ 'taxonomy' ] ) && $_POST[ 'taxonomy' ] != 'undefined' ? $_POST[ 'taxonomy' ] : 0;
	$offset = $paged*4;
	$paged++;

	if($taxonomy && $taxonomy == 'category'){
	    $term = get_queried_object();
		$children = get_terms(
			$taxonomy, [
				'parent' => $category,
				'hide_empty' => false,
				'offset' => $offset,
				'number' => 4,
			]
		);
		foreach ($children as $sub_category) :
			$image = get_field('image', $sub_category->taxonomy . '_' .$sub_category->term_id);
			$size = 'medium'; // (thumbnail, medium, large, full or custom size)
			$path = $image['sizes'][ $size ];
            ?>
            <div class="item__blog item__loaded" style="display: none;">
				<?php if($image) {?>
                    <img src="<?php echo esc_url($path); ?>">
				<?php } ?>
                <div class="blog__title"><a href="/category/<?=$sub_category->slug?>/"><h3><?=$sub_category->name?></h3></a></div>
                <div class="blog__text"><span><?=$sub_category->description?></span></div>
            </div> <?php
		endforeach;
    }
	else{
		$args = array(
			'cat' => $category, // ID of a page, post, or custom type
			'paged' => $paged,
			'posts_per_page' => 4,
			'post_status' => 'publish'
		);
		query_posts( $args );
		while( have_posts() ) : the_post();
			?>
            <div class="item__blog item__loaded" style="display: none;">
				<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
				<?php if(isset($large_image_url[0])) {?>
                    <img src="<?=$large_image_url[0]?>">
				<?php } ?>
                <div class="blog__date"><span><?php the_date(); ?></span></div>
                <div class="blog__title"><a href="<?php the_permalink();?>"><h3><?php the_title(); ?></h3></a></div>
                <div class="blog__text"><span><?= (get_field('job_title', get_the_ID()) ? get_field('job_title', get_the_ID()) : the_excerpt()); ?></span><?php the_date(); ?></div>
            </div>
		<?php
		endwhile;
    }

    die;
}

/**
 *
 */
function breadcrumbs() {

	/* === ОПЦИИ === */
	$text['home']     = 'Heim'; // текст ссылки "Главная"
	$text['category'] = '%s'; // текст для страницы рубрики
	$text['search']   = 'Suchergebnisse nach Abfrage "%s"'; // текст для страницы с результатами поиска
	$text['tag']      = 'Einträge mit Tag "%s"'; // текст для страницы тега
	$text['author']   = 'Artikel des Autors %s'; // текст для страницы автора
	$text['404']      = 'Fehler 404'; // текст для страницы 404
	$text['page']     = 'Seite %s'; // текст 'Страница N'
	$text['cpage']    = 'Seite kommentieren %s'; // текст 'Страница комментариев N'

	$wrap_before    = '<div class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">'; // открывающий тег обертки
	$wrap_after     = '</div><!-- .breadcrumbs -->'; // закрывающий тег обертки
	$sep            = '<span class="breadcrumbs__separator"> - </span>'; // разделитель между "крошками"
	$before         = '<span class="breadcrumbs__current">'; // тег перед текущей "крошкой"
	$after          = '</span>'; // тег после текущей "крошки"

	$show_on_home   = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
	$show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
	$show_current   = 1; // 1 - показывать название текущей страницы, 0 - не показывать
	$show_last_sep  = 1; // 1 - показывать последний разделитель, когда название текущей страницы не отображается, 0 - не показывать
	/* === КОНЕЦ ОПЦИЙ === */

	global $post;
	$home_url       = home_url('/');
	$link           = '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
	$link          .= '<a class="breadcrumbs__link" href="%1$s" itemprop="item"><span itemprop="name">%2$s</span></a>';
	$link          .= '<meta itemprop="position" content="%3$s" />';
	$link          .= '</span>';
	$parent_id      = ( $post ) ? $post->post_parent : '';
	$home_link      = sprintf( $link, $home_url, $text['home'], 1 );

	if ( is_home() || is_front_page() ) {

		if ( $show_on_home ) echo $wrap_before . $home_link . $wrap_after;

	} else {

		$position = 0;

		echo $wrap_before;

		if ( $show_home_link ) {
			$position += 1;
			echo $home_link;
		}

		if ( is_category() ) {
			$parents = get_ancestors( get_query_var('cat'), 'category' );
			foreach ( array_reverse( $parents ) as $cat ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
			}
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				$cat = get_query_var('cat');
				echo $sep . sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_current ) {
					if ( $position >= 1 ) echo $sep;
					echo $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
				} elseif ( $show_last_sep ) echo $sep;
			}

		} elseif ( is_search() ) {
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				if ( $show_home_link ) echo $sep;
				echo sprintf( $link, $home_url . '?s=' . get_search_query(), sprintf( $text['search'], get_search_query() ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_current ) {
					if ( $position >= 1 ) echo $sep;
					echo $before . sprintf( $text['search'], get_search_query() ) . $after;
				} elseif ( $show_last_sep ) echo $sep;
			}

		} elseif ( is_year() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . get_the_time('Y') . $after;
            elseif ( $show_home_link && $show_last_sep ) echo $sep;

		} elseif ( is_month() ) {
			if ( $show_home_link ) echo $sep;
			$position += 1;
			echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position );
			if ( $show_current ) echo $sep . $before . get_the_time('F') . $after;
            elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_day() ) {
			if ( $show_home_link ) echo $sep;
			$position += 1;
			echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position ) . $sep;
			$position += 1;
			echo sprintf( $link, get_month_link( get_the_time('Y'), get_the_time('m') ), get_the_time('F'), $position );
			if ( $show_current ) echo $sep . $before . get_the_time('d') . $after;
            elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_single() && ! is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$position += 1;
				$post_type = get_post_type_object( get_post_type() );
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->labels->name, $position );
				if ( $show_current ) echo $sep . $before . get_the_title() . $after;
                elseif ( $show_last_sep ) echo $sep;
			} else {
				$cat = get_the_category(); $catID = $cat[0]->cat_ID;
				$parents = get_ancestors( $catID, 'category' );
				$parents = array_reverse( $parents );
				$parents[] = $catID;
				foreach ( $parents as $cat ) {
					$position += 1;
					if ( $position > 1 ) echo $sep;
					echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
				}
				if ( get_query_var( 'cpage' ) ) {
					$position += 1;
					echo $sep . sprintf( $link, get_permalink(), get_the_title(), $position );
					echo $sep . $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after;
				} else {
					if ( $show_current ) echo $sep . $before . get_the_title() . $after;
                    elseif ( $show_last_sep ) echo $sep;
				}
			}

		} elseif ( is_post_type_archive() ) {
			$post_type = get_post_type_object( get_post_type() );
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->label, $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . $post_type->label . $after;
                elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_attachment() ) {
			$parent = get_post( $parent_id );
			$cat = get_the_category( $parent->ID ); $catID = $cat[0]->cat_ID;
			$parents = get_ancestors( $catID, 'category' );
			$parents = array_reverse( $parents );
			$parents[] = $catID;
			foreach ( $parents as $cat ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
			}
			$position += 1;
			echo $sep . sprintf( $link, get_permalink( $parent ), $parent->post_title, $position );
			if ( $show_current ) echo $sep . $before . get_the_title() . $after;
            elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_page() && ! $parent_id ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . get_the_title() . $after;
            elseif ( $show_home_link && $show_last_sep ) echo $sep;

		} elseif ( is_page() && $parent_id ) {
			$parents = get_post_ancestors( get_the_ID() );
			foreach ( array_reverse( $parents ) as $pageID ) {
				$position += 1;
				if ( $position > 1 ) echo $sep;
				echo sprintf( $link, get_page_link( $pageID ), get_the_title( $pageID ), $position );
			}
			if ( $show_current ) echo $sep . $before . get_the_title() . $after;
            elseif ( $show_last_sep ) echo $sep;

		} elseif ( is_tag() ) {
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				$tagID = get_query_var( 'tag_id' );
				echo $sep . sprintf( $link, get_tag_link( $tagID ), single_tag_title( '', false ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;
                elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_author() ) {
			$author = get_userdata( get_query_var( 'author' ) );
			if ( get_query_var( 'paged' ) ) {
				$position += 1;
				echo $sep . sprintf( $link, get_author_posts_url( $author->ID ), sprintf( $text['author'], $author->display_name ), $position );
				echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . sprintf( $text['author'], $author->display_name ) . $after;
                elseif ( $show_home_link && $show_last_sep ) echo $sep;
			}

		} elseif ( is_404() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			if ( $show_current ) echo $before . $text['404'] . $after;
            elseif ( $show_last_sep ) echo $sep;

		} elseif ( has_post_format() && ! is_singular() ) {
			if ( $show_home_link && $show_current ) echo $sep;
			echo get_post_format_string( get_post_format() );
		}

		echo $wrap_after;

	}
} // end of breadcrumbs()

/**
 * ACF создаем своё условие отображения форм на примере рубрик ( дополнительный параметр категории )
 * Скрипт написано по мотивам статьи https://wp-kama.ru/plugin/acf/dobavlenie-polej-k-opredelennoj-rubrike
 * и видео https://www.youtube.com/watch?v=mHJHnyPLf0M&t=39s
 */
// Создаем новое правило
add_action( 'admin_init', 'asp_acf_new_condition');
/**
 *
 */
function asp_acf_new_condition () {

	add_filter('acf/location/rule_types', 'asp_location_rules_types');

	function asp_location_rules_types($choices)
	{

		$key = __('Forms', 'acf');

		if ( ! isset( $choices[$key] ) )  {
			$choices[$key] = [];
		};

		$choices[$key][ 'category_id' ] = __('Category');

		// error_log(print_r($choices, 1));

		return $choices;
	}

// Формируем список значений которые может принимать правило
	add_filter( 'acf/location/rule_values/category_id', 'asp_location_rules_values_category_id');

	function asp_location_rules_values_category_id($choices)
	{

		$terms = get_terms( [
			'taxonomy' => 'category',
			'hide_empty' => false,
		]);

		if ($terms) {

			foreach ($terms as $term) {

				$choices[$term->term_id] = $term->name;
			}
		}

		//	error_log(print_r($terms, 1));

		return $choices;
	}

// Отображение ACF поля в нужном экране админки
	add_filter( 'acf/location/rule_match/category_id', 'asp_location_rules_match_category_id', 10, 3);
	function asp_location_rules_match_category_id($match, $rule, $options) {

		//error_log(print_r($rule, 1));

		$screen = get_current_screen();

		//error_log(print_r($screen, 1));

		if ( $screen->base !== 'term' || $screen->id !== 'edit-category') {
			return $match;
		}

		$term_id = $_GET[ 'tag_ID' ];
		$select_term = $rule[ 'value' ];

		if ( $rule[ 'operator' ] === '==') {
			$match = ( $term_id == $select_term );
		}
        elseif ($rule[ 'operator' ] === '!=') {
			$match = ( $term_id != $select_term );
		}

		return $match;
	}
}

/*
 * Popular Course
 *
 *
 * */
/**
 * @param $postID
 */
function setPostViews($postID) {
	$count_key = 'views';
	$count = get_post_meta( $postID, $count_key, true );
	if( $count == '' ){
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta( $postID, $count_key, '0' );
	} else {
		$count++;
		update_post_meta( $postID, $count_key, $count );
	}
}

/**
 * @param $postID
 * @return mixed|string
 */
function getPostViews($postID){
	$count_key = 'views';
	$count = get_post_meta( $postID, $count_key, true );
	if($count==''){
		delete_post_meta( $postID, $count_key );
		add_post_meta( $postID,	$count_key,	'0' );
		return	"0";
	}
	return	$count;
}
// добавляем запланированный хук
add_action('wp', 'my_activation');
/**
 *
 */
function my_activation() {
	if( ! wp_next_scheduled( 'my_daily_event' ) ) {
		wp_schedule_event( time(), 'daily', 'my_daily_event');
	}
}

// добавляем функцию к указанному хуку
/**
 *
 */
function do_this_daily() {
	global $wpdb;
	$postids = $wpdb->get_results("SELECT ID FROM $wpdb->posts WHERE post_status='publish' AND post_type='post' ORDER BY ID ASC");

	foreach( $postids as $postid ){
		$postid = $postid->ID; // ID записи

		// считаем количество просмотров
		$views = (int)get_post_meta( $postid, 'views', true );
		// считаем дни существования поста
		//$dtNow = get_the_time('U'); $dtTime = current_time('U'); $diff = $dtTime - $dtNow;
		$dtNow = get_post_time('U', true, $postid); $dtTime = current_time('U'); $diff = $dtTime - $dtNow;

		// считаем комментарии и сумму просмотров с комментариями
		$comments = get_comments_number( $postid );
		$summa = $views + $comments;
		// считаем индекс популярности
		if ( $days = '0' ){
			$pop_index = $summa / 1;
		} else {
			$days = (int)$diff/86400;
			$pop_index = $summa / $days;
		}
		$pop = round($pop_index, 2);
		// записываем индекс популярности в произвольное поле поста
		update_post_meta( $postid, 'popularity', $pop );
	}
}
add_action('my_daily_event', 'do_this_daily', 10, 2);

/**
 * true_duplicate_post_as_draft()
 * Функция создает дубликат поста в виде черновика и редиректит на его страницу редактирования
 * @return
 */
function true_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'true_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('Nothing to duplicate!');
	}

	/*
	 * получаем ID оригинального поста
	 */
	$post_id = (isset($_GET['post']) ? $_GET['post'] : $_POST['post']);
	/*
	 * а затем и все его данные
	 */
	$post = get_post( $post_id );

	/*
	 * если вы не хотите, чтобы текущий автор был автором нового поста
	 * тогда замените следующие две строчки на: $new_post_author = $post->post_author;
	 * при замене этих строк автор будет копироваться из оригинального поста
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;

	/*
	 * если пост существует, создаем его дубликат
	 */
	if (isset( $post ) && $post != null) {

		/*
		 * массив данных нового поста
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft', // черновик, если хотите сразу публиковать - замените на publish
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);

		/*
		 * создаем пост при помощи функции wp_insert_post()
		 */
		$new_post_id = wp_insert_post( $args );

		/*
		 * присваиваем новому посту все элементы таксономий (рубрики, метки и т.д.) старого
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // возвращает массив названий таксономий, используемых для указанного типа поста, например array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}

		/*
		 * дублируем все произвольные поля
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}


		/*
		 * и наконец, перенаправляем пользователя на страницу редактирования нового поста
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die("Post creation error, can't find original post with ID=: " . $post_id);
	}
}
add_action( 'admin_action_true_duplicate_post_as_draft', 'true_duplicate_post_as_draft' );

/**
 * true_duplicate_post_link()
 * Добавляем ссылку дублирования поста для post_row_actions
 * @param mixed $actions
 * @param mixed $post
 * @return
 */
function true_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="admin.php?action=true_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this post" rel="permalink">Duplicate</a>';
	}
	return $actions;
}

add_filter( 'post_row_actions', 'true_duplicate_post_link', 10, 2 );

/**
 * Добавляет секцию Управление блоками на главной
 */
add_action(/**
 * @param $customizer
 */ 'customize_register', function($customizer){
	$customizer->add_section(
		'section_main_blocks',
		array(
			'title' => 'H374 - Managing blocks on the home page',
			'description' => 'Managing blocks on the main page (hide/show, background)',
			'priority' => 35,
		)
	);
	$customizer->add_setting('home_top_banner');
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'home_top_banner',
			array(
				'label' => 'Top Banner on Home page (svg/png/jpg)',
				'section' => 'section_main_blocks',
				'settings' => 'home_top_banner'
			)
		)
	);

	$customizer->add_setting(
		'title_homepage_banner',
		array(
			'default' => '#',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'title_homepage_banner',
		array(
			'label' => 'Title top banner of Home page',
			'section' => 'section_main_blocks',
			'type' => 'text',
		)
	);
	$customizer->add_setting(
		'subtitle_homepage_banner',
		array(
			'default' => '#',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'subtitle_homepage_banner',
		array(
			'label' => 'SubTitle top banner of Home page',
			'section' => 'section_main_blocks',
			'type' => 'text',
		)
	);


	$customizer->add_setting('hide_gesichtspflege');
	$customizer->add_control(
		'hide_gesichtspflege',
		array(
			'type' => 'checkbox',
			'label' => 'Hide the Gesichtspflege section',
			'section' => 'section_main_blocks',
		)
	);
	$customizer->add_setting('image_gesichtspflege');
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'image_gesichtspflege',
			array(
				'label' => 'Image for Gesichtspflege section',
				'section' => 'section_main_blocks',
				'settings' => 'image_gesichtspflege'
			)
		)
	);

	$customizer->add_setting('hide_korperpflege');
	$customizer->add_control(
		'hide_korperpflege',
		array(
			'type' => 'checkbox',
			'label' => 'Hide the Körperpflege section',
			'section' => 'section_main_blocks',
		)
	);
	$customizer->add_setting('image_korperpflege');
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'image_korperpflege',
			array(
				'label' => 'Image for Körperpflege section',
				'section' => 'section_main_blocks',
				'settings' => 'image_korperpflege'
			)
		)
	);

	$customizer->add_setting('hide_behandlungen');
	$customizer->add_control(
		'hide_behandlungen',
		array(
			'type' => 'checkbox',
			'label' => 'Hide the Behandlungen section',
			'section' => 'section_main_blocks',
		)
	);

	$customizer->add_setting('hide_fur_sie');
	$customizer->add_control(
		'hide_fur_sie',
		array(
			'type' => 'checkbox',
			'label' => 'Hide the Für Sie section',
			'section' => 'section_main_blocks',
		)
	);
	$customizer->add_setting('image_for_frauen');
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'image_for_frauen',
			array(
				'label' => 'Image for Für Frauen section',
				'section' => 'section_main_blocks',
				'settings' => 'image_for_frauen'
			)
		)
	);
	$customizer->add_setting('image_for_manner');
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'image_for_manner',
			array(
				'label' => 'Image for Für Männer section',
				'section' => 'section_main_blocks',
				'settings' => 'image_for_manner'
			)
		)
	);

	$customizer->add_setting('hide_callback_block');
	$customizer->add_control(
		'hide_callback_block',
		array(
			'type' => 'checkbox',
			'label' => 'Hide the Callback section',
			'section' => 'section_main_blocks',
		)
	);
	$customizer->add_setting('image_callback_block');
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'image_callback_block',
			array(
				'label' => 'Image for Callback section',
				'section' => 'section_main_blocks',
				'settings' => 'image_callback_block'
			)
		)
	);
	$customizer->add_setting(
		'title_callback_block',
		array(
			'default' => '#',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'title_callback_block',
		array(
			'label' => 'Title for Callback block',
			'section' => 'section_main_blocks',
			'type' => 'text',
		)
	);
	$customizer->add_setting(
		'subtitle_callback_block',
		array(
			'default' => '#',
			'sanitize_callback' => 'check_sanitize_text',
			'transport' => 'postMessage',
		)
	);
	$customizer->add_control(
		'subtitle_callback_block',
		array(
			'label' => 'Title for Callback block',
			'section' => 'section_main_blocks',
			'type' => 'text',
		)
	);

	$customizer->add_setting('hide_schulungen');
	$customizer->add_control(
		'hide_schulungen',
		array(
			'type' => 'checkbox',
			'label' => 'Hide the Schulungen section',
			'section' => 'section_main_blocks',
		)
	);

	$customizer->add_setting('hide_unser_team');
	$customizer->add_control(
		'hide_unser_team',
		array(
			'type' => 'checkbox',
			'label' => 'Hide the Unser Team section',
			'section' => 'section_main_blocks',
		)
	);

	$customizer->add_setting('hide_partner');
	$customizer->add_control(
		'hide_partner',
		array(
			'type' => 'checkbox',
			'label' => 'Hide the Unsere Partner section',
			'section' => 'section_main_blocks',
		)
	);

	$customizer->add_setting('hide_blog');
	$customizer->add_control(
		'hide_blog',
		array(
			'type' => 'checkbox',
			'label' => 'Hide the Blog section',
			'section' => 'section_main_blocks',
		)
	);

	$customizer->add_setting('hide_contact');
	$customizer->add_control(
		'hide_contact',
		array(
			'type' => 'checkbox',
			'label' => 'Hide the Kontakt section',
			'section' => 'section_main_blocks',
		)
	);
});
