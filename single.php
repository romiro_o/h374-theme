<?php
/**
 * The template for displaying all single posts
 *
 * @package H374
 * @since H374 1.0
 */
get_header();
?>
<main class="single-page">
		<?php
		/* Start the Loop */
		while ( have_posts() ) : the_post(); ?>

        <!-- Section Breadcrumbs -->
        <section style="margin-bottom: 40px">
            <div class="container text__dark">
				<?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
            </div>
        </section>
        <!-- .Section Breadcrumbs -->

        <!-- Section Category Title -->
        <section class="page__title">
            <div>
                <h2><?php the_title();?><point>.</point></h2>
            </div>
        </section>
        <!-- .Section Category Title -->

        <section>
            <div class="container">
				<?php the_content(); ?>
            </div>
        </section>
		<?php endwhile; // End of the loop. ?>
	</main>
<?php setPostViews(get_the_ID()); ?>
<?php
get_footer();
