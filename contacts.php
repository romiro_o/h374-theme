<?php
/*
	Template Name: Contacts
	Template post type: page

 * @package H374
 * @since H374 1.0
 */
get_header();
?>
	<main class="single-page page__contacts">
		<?php
		/* Start the Loop */
		while ( have_posts() ) : the_post(); ?>

			<!-- Section Breadcrumbs -->
			<section>
				<div class="container text__light">
					<?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
				</div>
			</section>
			<!-- .Section Breadcrumbs -->

			<!-- Section Contact info-->
			<section class="section__contacts">
				<div class="container">
					<h2><?php the_title();?><point>.</point></h2>
					<div class="content__contacts">
						<?php the_content(); ?>
                    </div>
                    <div></div>
				</div>
                <div class="contact__map">
                    <div class="container">
                        <div class="contact__inner">
                            <div class="contact__inner__info">
                                <div class="contacts__info">
                                    <span><svg class="social__icon"><use xlink:href="#ico-watch"></use></svg><?= get_theme_mod('office_hours', '')?></span>
                                    <span><svg class="social__icon"><use xlink:href="#ico-placemark"></use></svg><?= get_theme_mod('address', '')?></span>
                                    <a href="tel:+49<?=str_replace(array(' ', '(' , ')', '-'), '', get_theme_mod('phone_number', ''))?>">
                                        <svg class="social__icon"><use xlink:href="#ico-phone"></use></svg><?= get_theme_mod('phone_number', '')?>
                                    </a>
                                </div>
                            </div>
                            <div class="contact__inner__form">
                                <div class="widget__callback">
                                    <span>Sie haben Fragen oder benötigen weitere Informationen?</span>
                                    <?php echo do_shortcode( '[ninja_form id=3]' ); ?>
								</div>
                            </div>
                        </div>
                    </div>
			        <?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                    <div id="map" style="background: url(<?=(!$large_image_url[0]) ?? '#eeeeee'?>)">
				        <?php if(!$large_image_url[0]):?>
                           <img src="<?=$large_image_url[0]?>">
				        <?php endif;?>
                        <style type="text/css">
                            .ymaps-layers-pane {
                                filter: invert(100%);
                                -ms-filter: invert(100%);
                                -webkit-filter: invert(100%);
                                -moz-filter: invert(100%);
                                -o-filter: invert(100%);
                            }
                        </style>

                        <script src="https://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU"></script>
                        <script>
                            ymaps.ready(init);
                            function init(){
                                var myMap = new ymaps.Map("map",{center: [53.563835, 10.057188],zoom: 12});
                                myMap.controls.add("zoomControl").add("typeSelector").add("mapTools");
                                var myPlacemark = new ymaps.Placemark([53.563835, 10.057188]);
                                myMap.geoObjects.add(myPlacemark);
                            }
                        </script>

                   </div>
                </div>
            </section>
			<!-- .Section Contact info -->


		<?php endwhile; // End of the loop. ?>
	</main>
<?php setPostViews(get_the_ID()); ?>
<?php
get_footer();
