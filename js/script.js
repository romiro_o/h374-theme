(function($) {
    $.fn.menumaker = function(options) {

        var cssmenu = $(this), settings = $.extend({
            title: "Menu",
            format: "dropdown",
            sticky: false
        }, options);

        return this.each(function() {
            $(".ico-mobil-menu").on('click', function(){
                var mainmenu = $('.top__menu #menu-header-menu');
                if (mainmenu.hasClass('open')) {
                    mainmenu.hide().removeClass('open');
                }
                else {
                    mainmenu.show().addClass('open');
                    if (settings.format === "dropdown") {
                        mainmenu.find('ul').show();
                    }
                }
            });

            cssmenu.find('#menu-header-menu li ul').parent().addClass('has-sub');

            multiTg = function() {
                cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
                cssmenu.find('.submenu-button').on('click', function() {
                    $(this).toggleClass('submenu-opened');
                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').hide();
                    }
                    else {
                        $(this).siblings('ul').addClass('open').show();
                    }
                });
            };

            if (settings.format === 'multitoggle') multiTg();
            else cssmenu.addClass('dropdown');

            if (settings.sticky === true) cssmenu.css('position', 'fixed');

            resizeFix = function() {
                if ($( window ).width() > 1024) {
                    cssmenu.find('ul').show();
                }

                if ($(window).width() <= 1024) {
                    cssmenu.find('ul').hide().removeClass('open');
                }
            };
            resizeFix();
            return $(window).on('resize', resizeFix);

        });
    };
})(jQuery);

(function($){
    $(document).ready(function(){

        $("#cssmenu").menumaker({
            title: "Menu",
            format: "multitoggle"
        });

    });
})(jQuery);

jQuery(document).ready(function($){
	/**/$("body").on('click', '.nav__link[href*="#"], .nav__link[href*="#"]', function(e){
    var fixed_offset = 40;
    $('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1000);
    e.preventDefault();
});
	
    $(".ico-mobil-menu").click(function () {
        $(".header__nav .top__menu").toggle("slow");
        this.classList.toggle("change");
        $('.header_menu').toggleClass("open-menu");
        $(".header__search").toggle("slow");
    });

    /* Options list for slider - home page */
    /**/$('.options ul li:has("div")').append('<span></span>');
    $('.options ul li a').click(function() {
        var checkElement = $(this).next(),
            visibleElement = $('ul div:visible');

        visibleElement.stop().animate({'height':'toggle'}, 500).parent().removeClass('active');
        if((checkElement.is('div')) && (!checkElement.is(':visible'))) {
            checkElement.stop().animate({'height':'toggle'}, 500).parent().addClass('active');
            return false;
        }
        if((checkElement.is('div')) && (checkElement.is(':visible'))) {
            return false;
        }
    });


	/*Load category image for - Behandlungen- section - home page */
    $( '[data-ajax-param]' ).click(function (e) {
        fetch_category(e);
    });
    function fetch_category(e){
        var param = $(e.target).attr('data-ajax-param');
        $.post('wp-admin/admin-ajax.php', {'action':'get_posts_by_category_id', 'param':param}, function(response){
            $('#post-img img').fadeOut(90, function() {    //для картинок
                setTimeout(function(){
                    $('#post-img img').attr('src', response.data[0]).fadeIn(1000);
                },600);
            });
            //$(e.target).parent('li').find('div').html(response.data[1]);
        });
    }

    /*$( '[data-ajax-param]' ).click(function (e) {
        fetch(e);
	});
    function fetch(e){
    	var param = $(e.target).attr('data-ajax-param');
		$.post('wp-admin/admin-ajax.php', {'action':'get_post_by_id', 'param':param}, function(response){
            $('#post-img img').fadeOut(500, function() {    //для картинок
					setTimeout(function(){
						 $('#post-img img').attr('src', response.data[0]).fadeIn(1000);
					},500);
			});
			//$(e.target).parent('li').find('div').html(response.data[1]);
        });
    }*/

    /* Team slider */
    let slider_team = $('.team__inner');
    slider_team.slick({
        lazyLoad: 'ondemand',
        slidesToShow: 3,
        slidesToScroll: $('.member__team').length > 0 ? 1 : 3,
        arrows:true,
        appendArrows:'.slider__arrows',
        prevArrow:'<span class="slider__arrow"><svg><use xlink:href="#ico-slider-arrow-prev"></use></svg></span>',
        nextArrow:'<span class="slider__arrow"><svg><use xlink:href="#ico-slider-arrow-next"></use></svg></span>',
        dots:false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: $('.member__team').length > 0 ? 1 : 2,
                },
            },
            {
                breakpoint: 1024,
                settings: {
                    centerMode: true,
                    centerPadding: '20px',
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows:false,
                    dots:true,
                }
            }
        ]
    });

    $('.slider__count__total').text( slider_team.slick("getSlick").slideCount);
    slider_team.on('afterChange', function(event, slick, currentSlide){
        $(".slider__count__current").text(currentSlide + 1);
    });

    //If Member of Team page
    if($('.member__team').length > 0){
        let member_id = $('.member__team').data('member-id');
        let slide_go = 1;
        $('.team__item[data-member-id="' + member_id + '"]').each(function (){
            if($(this).data('slick-index') > 0 ) {
                slide_go = $(this).data('slick-index'); return false;
            }
        })
        $('.team__item[data-slick-index="' + slide_go + '"]').addClass('selected');
        $('.team__inner').slick('slickGoTo', slide_go - 1 );
    }



    /* Partner slider */
    $(window).on('load resize', function() {
        if ($(window).width() < 1024) {
            $('.partner__inner:not(.slick-initialized)').slick({
                centerMode: true,
                centerPadding: '20px',
                slidesToShow: 2,
                slidesToScroll: 2,
                arrows:false,
                dots: true,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            centerMode: false,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]

            });
        } else {
            $(".partner__inner.slick-initialized").slick("unslick");
        }
    });

    /*  */
    let slider_procedure = $('.slider__inner');
    slider_procedure.slick({
        //lazyLoad: 'ondemand',
        slidesToShow: 1,
        arrows:true,
        appendArrows:'.slider__inner__nav',
        prevArrow:'<span class="slider__arrow"><svg><use xlink:href="#ico-slider-arrow-prev"></use></svg></span>',
        nextArrow:'<span class="slider__arrow"><svg><use xlink:href="#ico-slider-arrow-next"></use></svg></span>',
        dots:false,
        responsive: [
            /*{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },*/
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows:false,
                    dots:true,
                }
            }
        ]
    });
    $('.procedure__nav .slider__count__total').text( slider_procedure.slick("getSlick").slideCount);
    slider_procedure.on('afterChange', function(event, slick, currentSlide){
        $(".procedure__nav .slider__count__current").text(currentSlide + 1);
    });

    /**/
    /* More about training - slider */
    let training_slider = $('.more__training__slider');
    training_slider.slick({
        //lazyLoad: 'ondemand',
        slidesToShow: 1,
        arrows:true,
        appendArrows:'.more__training__arrows',
        prevArrow:'<span class="slider__arrow"><svg><use xlink:href="#ico-slider-arrow-prev"></use></svg></span>',
        nextArrow:'<span class="slider__arrow"><svg><use xlink:href="#ico-slider-arrow-next"></use></svg></span>',
        dots:false,
        responsive: [
            /**/{
                breakpoint: 1024,
                settings: {
                    centerMode: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
               },
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows:false,
                    dots:true,
                }
            }
        ]
    });
    $('.training__nav .slider__count__total').text( training_slider.slick("getSlick").slideCount);
    training_slider.on('afterChange', function(event, slick, currentSlide){
        $(".training__nav .slider__count__current").text(currentSlide + 1);
    });

    // определяем в переменные кнопку, текущую страницу и максимальное кол-во страниц
    var button = $( '#loadmore a' ),
        paged = button.data( 'paged' ),
        category = button.data( 'category' ),
        maxPages = button.data( 'max_pages' ),
        taxonomy = button.data( 'taxonomy' );


    button.click( function( event ) {
        event.preventDefault(); // предотвращаем клик по ссылке
        
        $.ajax({
            type : 'POST',
            url : '/wp-admin/admin-ajax.php',
            data : {
                category : category, // номер текущей категории
                paged : paged, // номер текущей страниц
                action : 'loadmore', // экшен для wp_ajax_ и wp_ajax_nopriv_
                taxonomy : taxonomy
            },
            beforeSend : function( xhr ) {
                button.text( 'Laden...' );
            },
            success : function( data ){
                paged++; // инкремент номера страницы
                button.closest('.container').find('.load__post').append( data );
                if (window.innerWidth <= 1024) {
                    button.closest('.container').find('.load__post .item__blog:nth-child(3), .load__post .item__blog:nth-child(4)').css('display', 'flex');
                }
                button.closest('.container').find('.load__post .item__blog').show('slow');
                button.text( 'Mehr herunterladen' );
                // если последняя страница, то удаляем кнопку
                if( paged == maxPages ) {
                    button.remove();
                }
            }
        });
    });

    //Show more Training
    /**/$('#Shulungen .more__button a').on('click', function (e) {
        e.preventDefault();
        $('#Shulungen .more__button').hide();
        $('.service__block').css('display','flex');
    });



    //Select service on category banner
    $('form.select_service').submit(function( event ) {
        event.preventDefault();
        window.location.href = $(this).find('option:selected').val();
    });

    $('input[name="tabs_gallery"]').on('click', function(){
        $(window).trigger("resize.twentytwenty").animate(1000);
    });

    $('.request__link').on('click', function(){
        var title = $('head title').text();
        var title_section  = $(this).data('title');
        if(!$(this).closest('section').hasClass('top__banner') && !title_section){
            title_section  = $(this).closest('section').find('h2').text();
        }
        else if(title_section){
            $('#contact_form_pop h2').text(title_section);
        }

        $('#contact_form_pop').find('input[type="hidden"]').val(title + ' ' + title_section);
    })





});

function initMap() {
    // Styles a map in night mode.
    const map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: 40.674, lng: -73.945 },
        zoom: 12,
        styles: [
            { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
            { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
            { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
            {
                featureType: "administrative.locality",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
            },
            {
                featureType: "poi",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
            },
            {
                featureType: "poi.park",
                elementType: "geometry",
                stylers: [{ color: "#263c3f" }],
            },
            {
                featureType: "poi.park",
                elementType: "labels.text.fill",
                stylers: [{ color: "#6b9a76" }],
            },
            {
                featureType: "road",
                elementType: "geometry",
                stylers: [{ color: "#38414e" }],
            },
            {
                featureType: "road",
                elementType: "geometry.stroke",
                stylers: [{ color: "#212a37" }],
            },
            {
                featureType: "road",
                elementType: "labels.text.fill",
                stylers: [{ color: "#9ca5b3" }],
            },
            {
                featureType: "road.highway",
                elementType: "geometry",
                stylers: [{ color: "#746855" }],
            },
            {
                featureType: "road.highway",
                elementType: "geometry.stroke",
                stylers: [{ color: "#1f2835" }],
            },
            {
                featureType: "road.highway",
                elementType: "labels.text.fill",
                stylers: [{ color: "#f3d19c" }],
            },
            {
                featureType: "transit",
                elementType: "geometry",
                stylers: [{ color: "#2f3948" }],
            },
            {
                featureType: "transit.station",
                elementType: "labels.text.fill",
                stylers: [{ color: "#d59563" }],
            },
            {
                featureType: "water",
                elementType: "geometry",
                stylers: [{ color: "#17263c" }],
            },
            {
                featureType: "water",
                elementType: "labels.text.fill",
                stylers: [{ color: "#515c6d" }],
            },
            {
                featureType: "water",
                elementType: "labels.text.stroke",
                stylers: [{ color: "#17263c" }],
            },
        ],
    });
}