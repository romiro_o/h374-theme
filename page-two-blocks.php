<?php
/*

Template Name: Our Team
Template post type: post, page

*/
get_header(); ?>

<?php
$category = get_queried_object();
$title_page = get_field('title_page', $category->taxonomy . '_' . $category->term_id)
	? get_field('title_page', $category->taxonomy . '_' . $category->term_id) : '';

?>
<?php get_header();?>
<main class="single-page our__team">
    <!-- Section Breadcrumbs -->
    <section style="margin-bottom: 40px">
        <div class="container">
			<?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
        </div>
    </section>
    <!-- .Section Breadcrumbs -->

    <!-- Section Our Team -->
    <script>
        jQuery(document).ready(function($) {

        });
    </script>
	<?php get_template_part( 'template-parts/section-our-team'); ?>
    <!-- .Section Our Team -->

    <!-- Section Member -->
    <section class="member__team" data-member-id="<?= get_the_ID(); ?>">
        <div class="container">
            <div class="member__inner">
                <div class="member__img">
					<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                    <img src="<?=$large_image_url[0]?>">
                </div>
                <div class="member__description">
                    <div class="member__text">
                        <h2><?php the_title();?></h2>
                        <div class="member__job"><?php the_field('job_title')?></div>
                        <div class="member__content"><?php the_content(); ?></div>
                    </div>
                    <?php if(have_rows('skills')){ ?>
                    <div class="member__counter">
	                <?php while ( have_rows('skills') ) : the_row();?>
                        <div class="member__counter__item">
                            <div class="data__counter"><?= get_sub_field('data_counter'); ?></div>
                            <div class="description__counter"><?= get_sub_field('description_counter'); ?></div>
                        </div>
	                <?php endwhile; ?>
                        <div class="our__team_background"></div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>

    </section>
    <!-- .Section Member -->

    <!-- Section Member Diplomas -->
    <?php
    $images = get_field('member_diplomas');
    if( $images ): ?>
    <section class="member__diplomas">
        <div class="container">
            <h2>Diplome<point>.</point></h2>
                <div class="diplomas__img">
					<?php foreach( $images as $image ): ?>
                        <a href="<?php echo esc_url($image['url']); ?>" data-fancybox="images" data-caption="">
                            <img src="<?php echo esc_url($image['sizes']['medium']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                        </a>
                    <?php endforeach; ?>
                </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- .Section Member Diplomas -->

    <!-- Section Member Areas Activity -->
	<?php
	$featured_posts = get_field('areas_of_activity');
	if( $featured_posts ): ?>
    <section class="page__inner areas__activity ">
        <div class="container">
            <h2>Tätigkeitsbereiche<point>.</point></h2>
            <div class="page__cards">

					<?php foreach( $featured_posts as $post ): setup_postdata($post); ?>
                <a class="page__card" href="<?php the_permalink();?>">
                    <div>
						<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' ); ?>
						<?php if(is_array($large_image_url) && $large_image_url[0]) : ?>
                            <img src="<?=$large_image_url[0]?>">
						<?php endif; ?>
                        <span class="card__title"><?php the_title();?></span>
						<?php
						$value = get_field( "description_for_category_page" );
						if( $value ) {
							echo '<div class="card__caption">' . $value . '</div>';
						}
						?>
                    </div>
                </a>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>

            </div>
        </div>
    </section>
	<?php endif; ?>
    <!-- .Section Member Areas Activity -->

    <!-- Section  Blog -->
	<?php get_template_part( 'template-parts/section-blog' ); ?>
    <!-- .Section Blog -->

    <!-- Section  Information -->
	<?php get_template_part( 'template-parts/section-information' ); ?>
    <!-- .Section Information -->
</main>
<?php
get_footer();
