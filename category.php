<?php
/**
 * The template for displaying archive pages
 *
 * @package H374
 * @since H374 1.0
 */

get_header();
$taxonomy = 'category';
$term = get_queried_object();
$children = get_terms(
$term->taxonomy, [
		'parent' => $term->term_id,
		'hide_empty' => false,
		//'number' => 4,
		'posts_per_page' => -1,
	]
);

$template = get_field('template', $term->taxonomy . '_' .$term->term_id);

if($template && $template  != 'Default'){
	?>
	<!-- Section Gesichtspflege -->
	<?php get_template_part( 'template-parts/category-' . $template ); ?>
	<!-- .Section Gesichtspflege -->
	<?php

} else{
?>

	<section id="Blog" class="blog category__list" style="margin-bottom: 40px">
	<div class="container text__dark">
		<?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
        <h2><?=$term->name;?><point style="color: #EBAC21;">.</point></h2>
        <div>
            <h3><?php echo (get_field('headline_1', $term->taxonomy . '_' .$term->term_id)) ; ?></h3>
            <div><?php echo(get_field('text_1', $term->taxonomy . '_' .$term->term_id)) ; ?></div>
            <h3><?php echo(get_field('headline_2', $term->taxonomy . '_' .$term->term_id)) ; ?></h3>
            <div><?php echo(get_field('text_2', $term->taxonomy . '_' .$term->term_id)) ; ?></div>
        </div>
        <div class="blog__inner load__post">

<?php
if($children){ ?>
    <?php foreach ($children as $sub_category) : ?>
		<?php
		$image = get_field('image', $sub_category->taxonomy . '_' .$sub_category->term_id);
		$size = 'large'; // (thumbnail, medium, large, full or custom size)
		$path = (is_array($image) && isset($image['sizes'][ $size ])) ? $image['sizes'][ $size ] : '';
		?>
        <div class="item__blog">
            <?php if($path) {?>
                <a href="/category/<?=$sub_category->slug?>/"><img src="<?php echo esc_url($path); ?>"></a>
           <?php } ?>
            <div class="blog__title"><a href="/category/<?=$sub_category->slug?>/"><h3><?=$sub_category->name?></h3></a></div>
            <div class="blog__text"><span><?=$sub_category->description?></span></div>
        </div>
    <?php endforeach; } else {
	    $wpb_all_query = new WP_Query(
	        [
				'cat' => $term->term_id,
                'post_type'=>'post',
                'post_status'=>'publish',
                'posts_per_page'=>-1
            ]);
	?>

	<?php if ( $wpb_all_query->have_posts() ) : while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); $taxonomy = 'post';?>
        <div class="item__blog">
			<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
		<?php if(isset($large_image_url[0])) {?>
            <a href="<?php the_permalink();?>"><img src="<?=$large_image_url[0]?>"></a>
		<?php } ?>
            <div class="blog__date"><span><?php the_date(); ?></span></div>
            <div class="blog__title"><a href="<?php the_permalink();?>"><h3><?php the_title(); ?></h3></a></div>
            <div class="blog__text"><span><?= (get_field('job_title', get_the_ID()) ? get_field('job_title', get_the_ID()) : the_excerpt()); ?></span></div>
        </div>
	<?php endwhile; ?>
	<?php else : ?>
		<div>Inhalt keine</div>
	<?php endif; ?>
<?php } ?>
        </div>
		<?php
		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		/*
		$max_pages = ($term->count) ? ceil($term->count/4)
            : ($children ? (ceil(wp_count_terms( 'category', ['parent' => $term->term_id])/4)) : 5);
		if($paged < $max_pages || count($children) > 4) {
		echo '<div class="more__button" id="loadmore" >
		            <a href="#" data-taxonomy="' . $taxonomy . '" data-category="' . $term->term_id . '" data-max_pages="' . $max_pages . '" data-paged="' . $paged . '">Mehr erfahren</a>
	             </div>';
		}*/
		?>
	</div>
	</section>
<?php } ?>
<?php wp_reset_postdata();?>
<?php get_footer();