<?php
/*

Template Name: Procedure
Template post type: post, page

*/
get_header();
?>
<?php
$category = get_queried_object();
$term = get_category( $category->parent );
$params = [
	'term' => $term,
	'current' => $category
];
?>
<main class="single-page">
    <!-- Section Top Banner -->
	<?php get_template_part( 'template-parts/section-post-banner', '', ['button_title' => 'Vereinbaren Sie einen Termin'] ); ?>
    <!-- .Section Top Banner -->
    <!-- Section Category Title -->
    <section class="page__title">
        <div>
            <h2><?php the_title();?><point>.</point></h2>
        </div>
    </section>
    <!-- .Section Category Title -->


    <!-- Section Post Tabs -->
    <?php if(have_rows('tabs')){ ?>
    <section>
        <div class="container">
            <div class="tabset">
		    <?php	// loop through the rows of data
		    $tabs_title = $tabs_content = ''; $i = 1;
		    while ( have_rows('tabs') ) : the_row();
		        $tabs_title .= '<!-- Tab ' . $i . ' -->
                    <input type="radio" name="tabset" id="tab' . $i . '" aria-controls="' . $post->post_name . $i . '" '
                    . ($i == 1 ? 'checked="checked"' : '' ) .'>
                    <label for="tab' . $i . '">'. get_sub_field('title_tab') .'</label>';

		        $tabs_content .= '<section id="' . $post->post_name . $i . '" class="tab-panel">'. get_sub_field('content_tab') .'</section>';
            ?>
            <?php ++$i; endwhile; ?>
                <?= $tabs_title; ?>
                <div class="tab-panels">
					<?= $tabs_content; ?>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    <!-- .Section Post Tabs -->

    <!-- Section Post Tabs Gallery -->
	<?php get_template_part( 'template-parts/section-tabs-gallery'); ?>
    <!-- .Section Post Tabs -->

    <!-- Section Block Price
    <?php if(have_rows('block_price')){ ?>
    <section class="prices">
        <div class="container">
            <h2>Preise.</h2>
            <div class="prices__inner">
                <?php while ( have_rows('block_price') ) : the_row(); ?>
                    <div class="price__item">
                        <div class="price__procedure"><?=get_sub_field('procedure');?></div>
                        <div class="price__cost"><?=get_sub_field('cost');?></div>
                    </div>
                <?php endwhile; ?>
            </div>
            <div class="prices__button">
                <a href="#contact_form_pop" class="request__link fancybox-inline">Vereinbaren Sie einen Termin</a>
            </div>
            <div class="prices__background"></div>
        </div>
    </section>
    <?php } ?>-->
    <!-- .Section Block Price -->
    <!-- Section Post Content-->
    <?php
	$table = get_field('table_price');
	$thead = [];
	if ($table || have_rows('block_price')){ ?>
    <section class="prices" >
        <div class="container">
            <h2>Preise.</h2>
            <?php if (have_rows('block_price')){ ?>
            <div class="prices__inner">
				<?php while ( have_rows('block_price') ) : the_row(); ?>
                    <div class="price__item">
                        <div class="price__procedure"><?=get_sub_field('procedure');?></div>
                        <div class="price__cost"><?=get_sub_field('cost');?></div>
                    </div>
				<?php endwhile; ?>
            </div>
            <?php } ?>
			<?php
		if ($table) {
			echo '<div class="prices__inner table__type">';
			if ($table['caption']) {
				echo '<caption>' . $table['caption'] . '</caption>';
			}
			if ($table['header']) {
				$i = 0;
				echo '<div class="price__item table__head">';
				foreach ($table['header'] as $th) {
					$class_column = !$i ? 'price__procedure' : 'price__cost';
					echo '<div class="' . $class_column . '">';
					$thead[$i] = $th['c'];
					echo $th['c'];
					echo '</div>';
					++$i;
				}
				echo '</div>';
			}

			foreach ($table['body'] as $tr) {
				$i = 0;
				echo '<div class="price__item">';
				foreach ($tr as $td) {
					$class_column = !$i ? 'price__procedure' : 'price__cost';
					echo '<div class="' . $class_column . '">';
					if ($thead && $i) {
						echo '<span>';
						echo $thead[$i];
						echo ':</span>';
					}
					echo $td['c'];
					echo '</div>';
					++$i;
				}
				echo '</div>';
			}
			echo '</div>';
		} ?>
            <div class="prices__button">
                <a href="#contact_form_pop" class="request__link fancybox-inline">Vereinbaren Sie einen Termin</a>
            </div>
            <div class="prices__background"></div>
        </div>
    </section>
	<?php } ?>
    <!-- .Section Post Content -->

    <!-- Section Post Content-->
    <section>
        <div class="container">
			<?php the_content(); ?>
        </div>
    </section>
    <!-- .Section Post Content -->
	<!-- Section Our Team -->
<?php get_template_part( 'template-parts/section-our-team' ); ?>
	<!-- .Section Our Team -->

	<!-- Section Callback-block -->
<?php get_template_part( 'template-parts/section-callback-block' ); ?>
	<!-- .Section Callback-block -->

	<!-- Section  Blog -->
<?php get_template_part( 'template-parts/section-blog' ); ?>
	<!-- .Section Blog -->

	<!-- Section  Information -->
<?php get_template_part( 'template-parts/section-information' ); ?>
	<!-- .Section Information -->

</main>

<?php
get_footer();
