<?php
/**
 * The template for displaying archive pages
 *
 * @package H374
 * @since H374 1.0
 */

get_header();

$description = get_the_archive_description();
?>

<?php if ( have_posts() ) : ?>

	<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
	<?php if ( $description ) : ?>
        <div class="archive-description"><?php echo wp_kses_post( wpautop( $description ) ); ?></div>
	<?php endif; ?>

	<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
	<?php endwhile; ?>

<?php else : ?>
	<div>Inhalt keine</div>
<?php endif; ?>

<?php get_footer(); ?>
