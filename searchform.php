<?php
/**
 * The searchform.php template.
 *
 * Used any time that get_search_form() is called.
 * @package H374
 * @since H374 1.0
 */

/*
 * Generate a unique ID for each form and a string containing an aria-label
 * if one was passed to get_search_form() in the args array.
 */
$h374_unique_id = wp_unique_id( 'search-form-' );

?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="<?php echo esc_attr( $h374_unique_id ); ?>"></label>
	<input type="search" id="<?php echo esc_attr( $h374_unique_id ); ?>" placeholder="Suche" class="search-field" value="<?php echo get_search_query(); ?>" name="s" />
	<input type="submit" class="search-submit" value="" />
</form>
