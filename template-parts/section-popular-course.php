<?php
if(isset($args['term_id'])){
	$category_id = $args['term_id'];
	$title_block = $args['title_block'];
}
else{
	$category = get_the_category();
	$category_id = $category[0]->term_id;
	$title_block = 'Das könnte Sie auch interessieren';
}
?>
<section class="popular__posts service">
    <div class="container">
        <h2><?= $title_block;?><point>.</point></h2>
        <div class="service_inner">
			<?php
			$args = array(
				'cat' => $category_id,
				'meta_query'     => array(
					'meta_value_num' => array(
						'key'	=> 'popularity'
					),
				),
				'orderby'            => 'meta_value_num',
				'posts_per_page'     => 4,
				'post_status'        => 'publish',
				'order'              => 'DESC'
			);
			$query = new WP_Query( $args );
			while ($query->have_posts()) : $query->the_post();
				$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );

				?>
                <div class="service__block">
					<?php if(is_array($large_image_url) && $large_image_url[0]) : ?>
                        <img src="<?=$large_image_url[0]?>">
					<?php endif; ?>
                    <a href="<?php the_permalink();?>"><div><span><?php the_title();?></span></div></a>
                </div>
			<?php endwhile; ?>
        </div>
    </div>
</section>