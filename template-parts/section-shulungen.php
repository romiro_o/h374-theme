<?php
if(isset($args['term_id'])){
$category_id = $args['term_id'];
	$category =	get_category($category_id);
}
else{
	$category = get_queried_object();
	$category_id = $category->term_id;
}
$param = array(
	'cat' =>  $category_id,
	'posts_per_page' => -1,
	'post_status' => 'publish',
	'meta_key'			=> 'sort',
	'orderby'			=> 'meta_value',
	'order'				=> 'ASC'

);
$text_block = '<div class="service__block text__block">
                <span>' . $category->category_description . '</span>
            </div>';
query_posts( $param );
?>
<?php if(have_posts()): ?>
<section id="Shulungen" class="service">
    <div class="container">
        <h2>Schulungen<point style="color: #EBAC21;">.</point></h2>
        <div class="service_inner">
            <?php $i = 0; while ( have_posts() ) : the_post();
				$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
				if($i == 5){
				    echo $text_block;
                }
				?>
                <div class="service__block">
					<?php if(is_array($large_image_url) && $large_image_url[0]) : ?>
                        <a class="img__link" href="<?php the_permalink();?>"><img src="<?=$large_image_url[0]?>"></a>
					<?php endif; ?>
                    <a href="<?php the_permalink();?>"><div><span><?php the_title();?></span></div></a>
                </div>
                <?php ++$i; endwhile; ?>
        </div>
        <div class="more__button"><a href="#">Alles sehen</a></div>
    </div>
</section>
<?php endif; ?>