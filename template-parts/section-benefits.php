<?php
$benefits_filed = 'course_benefits';
$term_id = '';
$image_benefits = 'benefits_image_block';
if(isset($args['term'])){
	$category = $args['term'];
	$term_id = $category->taxonomy . '_' . $category->term_id;
	$benefits_filed = 'block_benefits_of_our_training';
	$image_benefits = 'image_block';
}
?>
<?php if(have_rows($benefits_filed, $term_id)){ ?>
	<section class="benefits">
		<div class="container">
			<h2><?= get_field('title_block_benefits', $term_id);?><point>.</point></h2>
			<div class="section__inner">
				<?php while ( have_rows($benefits_filed, $term_id) ) : the_row();?>
					<div class="benefit__card">
						<img src="<?= get_sub_field('ico_benefit')['sizes'][ 'large' ];?>">
						<h3><?= get_sub_field('title_benefit');?></h3>
						<div class="benefit__content"><?= get_sub_field('content_benefit');?></div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
		<?php if(get_field($image_benefits, $term_id)){
			$image_banner = get_field($image_benefits, $term_id);
			$image_src = $image_banner['sizes'][ 'large' ];?>
			<div class="benefits__image">
				<img src="<?= $image_src;?>">
			</div>
		<?php } ?>
	</section>
<?php } ?>
