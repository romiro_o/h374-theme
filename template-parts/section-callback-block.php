<?php
$title = get_theme_mod('title_callback_block', '');
$sub_title = get_theme_mod('subtitle_callback_block', '');
$image_src = get_theme_mod('image_callback_block', '');
if(isset($args['term'])){
	$title = 'Kostenlose Beratung anfordern';
	$sub_title = 'Um die kostenlose Beratung zu erhalten, hinterlassen Sie uns einfach Ihre Telefonnummer';
	$image = get_field('image_for_callback_block', $args['current']->taxonomy . '_' . $args['current']->term_id);
	if($image){
		$size = 'large'; // (thumbnail, medium, large or custom size)
		$image_src = $image['sizes'][ $size ];
    }
	$title = !get_field('title_for_callback_block', $args['current']->taxonomy . '_' . $args['current']->term_id)
        ? $title : get_field('title_for_callback_block', $args['current']->taxonomy . '_' . $args['current']->term_id);
	$sub_title = !get_field('subtitle_for_callback_block', $args['current']->taxonomy . '_' . $args['current']->term_id)
        ? $sub_title : get_field('subtitle_for_callback_block', $args['current']->taxonomy . '_' . $args['current']->term_id);

}

?>
<section class="forth">
	<div class="container">
		<div class="section__inner">
			<h2><?=$title;?><point style="color: #EBAC21;">.</point></h2>
			<div class="section__form">
				<div class="widget__callback">
					<span><?=$sub_title;?></span>
					<?php echo do_shortcode( '[ninja_form id=4]' ); ?>
				</div>
			</div>
			<div class="section__img">
				<img src="<?=$image_src;?>">
			</div>
		</div>

	</div>
</section>
