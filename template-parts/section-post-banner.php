<?php
/*
* $args array
*/
$title = $label = $description = $class_banner_image = $class_price_from = $sub_description = '';
$widget_form = $form_callback = false;
$children = $image = [];
$button_title = 'Vereinbaren Sie einen Termin';
if(isset($args['button_title'])){
	$button_title = $args['button_title'];
}
if(isset($args['term'])){
	$widget_form = true;
	if(isset($args['current']) && get_field('template', $args['current']->taxonomy . '_' . $args['current']->term_id) == 'Fur-Sie'){
		$image = get_field('image', $args['current']->taxonomy . '_' . $args['current']->term_id);
		$title =$args['current']->name;
		$description = $args['current']->description;
		$sub_description = get_field('title_before_select_on_banner', $args['current']->taxonomy . '_' . $args['current']->term_id);
		$label = get_field('price_from', $args['current']->taxonomy . '_' . $args['current']->term_id);
		$class_banner_image = 'full_image';
	}

	$image = !$image ? get_field('image', $args['term']->taxonomy . '_' . $args['term']->term_id) : $image;
	$size = 'large'; // (thumbnail, medium, large or custom size)
	$image_src = $image['sizes'][$size] ?? null;
	$children = get_terms(
		$args['term']->taxonomy, [
			'parent' => $args['term']->term_id,
			'hide_empty' => false,
			'meta_key'			=> 'sort',
			'orderby'			=> 'meta_value',
			'order'				=> 'ASC'
		]
	);

	$title = !$title ? $args['term']->name : $title;
	$description = !$description ? $args['term']->description : $description;

	$sub_description = !$sub_description ? get_field('title_before_select_on_banner', $args['term']->taxonomy . '_'
        . $args['term']->term_id) : $sub_description;
	$price_term = get_field('price_from', $args['term']->taxonomy . '_' . $args['term']->term_id);
	if($label || $price_term){
		$class_price_from = 'price_from';
	}

	$label = !$label ? ($price_term ? get_field('price_from', $args['term']->taxonomy . '_' . $args['term']->term_id)
        : '') : $label;
}
else{
	$widget_form = $form_callback = true;
	$title = get_the_title();
	$description = get_field('description_banner');
	$label = get_field('price_from');
	$class_price_from = 'price_from';
	if(get_field('banner_image')){
		$image_banner = get_field('banner_image');
		$image_src = $image_banner['sizes'][ 'large' ] ?? null;
	}
	else{
		$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		$image_src = $large_image_url[0] ?? null;
	}
}


?>
<section class="top__banner page__banner post__banner">
	<div class="container">
		<div class="top__banner__inner">
			<div class="top__banner__text">
				<div>
					<?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
					<h1><?=$title; ?><point style="color: #EBAC21;">.</point></h1>
					<?php if($description){ ?>
						<span><?=$description;?></span>
					<?php }?>
					<?php if($sub_description){ ?>
                        <span class="banner__sub__description"><?=$sub_description;?></span>
					<?php }?>
					<?php if($widget_form){ ?>
						<div class="widget__callback">
							<?php if($label){ ?>
								<span class="<?=$class_price_from;?>"><?=$label;?></span>
							<?php }?>
							<div>
								<a href="#contact_form_pop" data-title="<?= $button_title; ?>" class="request__link fancybox-inline"><?= $button_title; ?></a>
							</div>
						</div>
					<?php } ?>
					<?php if(!isset($args['term']) && have_rows('sub_description_on_the_banner')){ ?>
						<div class="sub__description">
							<?php	// loop through the rows of data
							while ( have_rows('sub_description_on_the_banner') ) : the_row();
								?>
								<div>
									<span class="title__column"><?php the_sub_field('title_column'); ?></span>
									<span class="description__column"><?php the_sub_field('description_column'); ?></span>
								</div>
							<?php endwhile; ?>
						</div>
					<?php } ?>
				</div>

			</div>
		</div>

	</div>
	<?php if($image_src){ ?>
		<div class="top__banner__img <?=$class_banner_image?>">
			<div>
				<img src="<?= esc_url($image_src); ?>">
			</div>
		</div>
	<?php } ?>
</section>