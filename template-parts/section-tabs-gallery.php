<?php
/*
* $args array
*/
$tabs_title = $tabs_content = ''; $i = 1;
$cat = get_the_category(get_the_ID());
if(have_rows('tabs_gallery') || have_rows('tabs_gallery_post', $cat[0]->taxonomy . '_' . $cat[0]->term_id)){ ?>
	<section>
		<div class="container">
			<div class="tabset">
				<?php	// loop through the rows of data
                if(have_rows('tabs_gallery_post', $cat[0]->taxonomy . '_' . $cat[0]->term_id)){
				    while ( have_rows('tabs_gallery_post', $cat[0]->taxonomy . '_' . $cat[0]->term_id) ) : the_row();
					$tabs_title .= '<!-- Tab ' . $i . ' -->
                    <input type="radio" name="tabs_gallery" id="tabs_gallery' . $i . '" aria-controls="tabs_gallery_' . $post->post_name . $i . '" '
						. ($i == 1 ? 'checked="checked"' : '' ) .'>
                    <label for="tabs_gallery' . $i . '">'. get_sub_field('title_tab') .'</label>';
					//if gallery
					if(strpos(get_sub_field('content_tab'), 'wp-image-') !== FALSE){
						preg_match_all('/<img[^>]+src="?\'?([^"\']+)"?\'?[^>]*>/i', get_sub_field('content_tab'), $images, PREG_SET_ORDER);
						$gallery = '';
						foreach ($images as $image) {
							$gallery .= '<div class="apparate__img">
                        <a href="'.$image[1].'" data-fancybox="images" data-caption="">
                            <img src="'.$image[1].'" alt="" />
                        </a>

					</div>';
						}
						$tabs_content .= '<section id="tabs_gallery_' . $post->post_name . $i . '" class="tab-panel apparate"><div class="apparate__inner">'.$gallery.'</div></section>';
					} // if Before After slider
					else{
						$tabs_content .= '<section id="tabs_gallery_' . $post->post_name . $i . '" class="tab-panel">'. do_shortcode( get_sub_field('content_tab'), false ) .'</section>';
					}
					 ++$i; endwhile;
                }
				if(have_rows('tabs_gallery')){
					while ( have_rows('tabs_gallery') ) : the_row();
						$tabs_title .= '<!-- Tab ' . $i . ' -->
                    <input type="radio" name="tabs_gallery" id="tabs_gallery' . $i . '" aria-controls="tabs_gallery_' . $post->post_name . $i . '" '
							. ($i == 1 ? 'checked="checked"' : '' ) .'>
                    <label for="tabs_gallery' . $i . '">'. get_sub_field('title_tab') .'</label>';
						//if gallery
						if(strpos(get_sub_field('content_tab'), 'wp-image-') !== FALSE){
							preg_match_all('/<img[^>]+src="?\'?([^"\']+)"?\'?[^>]*>/i', get_sub_field('content_tab'), $images, PREG_SET_ORDER);
							$gallery = '';
							foreach ($images as $image) {
								$gallery .= '<div class="apparate__img">
                        <a href="'.$image[1].'" data-fancybox="images" data-caption="">
                            <img src="'.$image[1].'" alt="" />
                        </a>

					</div>';
							}
							$tabs_content .= '<section id="tabs_gallery_' . $post->post_name . $i . '" class="tab-panel apparate"><div class="apparate__inner">'.$gallery.'</div></section>';
						} // if Before After slider
						else{
							$tabs_content .= '<section id="tabs_gallery_' . $post->post_name . $i . '" class="tab-panel">'. do_shortcode( get_sub_field('content_tab'), false ) .'</section>';
						}
						 ++$i; endwhile;
				}
				?>
				<?= $tabs_title; ?>
				<div class="tab-panels">
					<?= $tabs_content; ?>
				</div>
			</div>
		</div>
	</section>

<?php } ?>