<section id="Fur_Sie" class="third">
	<div class="container">
		<div class="section__inner">
			<h2>Für Sie<point style="color: #EBAC21;">.</point></h2>
			<div class="section__posts">
				<a class="post__info" href="/category/fur-sie/fur-frauen/">
					<img src="<?= get_theme_mod('image_for_frauen', '')?>">
					<span class="post__title">
							<span>Für Frauen</span>
							<svg class="simple__icon"><use xlink:href="#ico-arrow-next"></use></svg>
						</span>
				</a>
				<a class="post__info" href="/category/fur-sie/fur-manner/">
					<img src="<?= get_theme_mod('image_for_manner', '')?>">
					<span class="post__title">
							<span>Für Männer</span>
							<svg class="simple__icon"><use xlink:href="#ico-arrow-next"></use></svg>
						</span>
				</a>
			</div>
		</div>
	</div>
</section>