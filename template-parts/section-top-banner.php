<section class="top__banner">
    <div class="container">
        <div class="top__banner__inner">
            <div class="top__banner__img">
                <img src="<?= get_theme_mod('home_top_banner', '')?>">
            </div>
            <div class="top__banner__text">
                <div>
                    <h1><?= get_theme_mod('title_homepage_banner', '')?><point style="color: #EBAC21;">.</point></h1>
                    <span><?= get_theme_mod('subtitle_homepage_banner', '')?><point style="color: #EBAC21;">.</point></span>
                </div>
            </div>
        </div>
    </div>
</section>