<section id="Information" class="contacts">
	<div class="contacts__img">
		<img src="<?=get_template_directory_uri();?>/images/contacts-main.jpg">
	</div>
	<div class="contacts__inner">
		<div class="contacts__text">
			<h2>Kontakt<point style="color: #EBAC21;">.</point></h2>
			<div class="contacts__info">
				<span><svg class="social__icon"><use xlink:href="#ico-watch"></use></svg><?= get_theme_mod('office_hours', '')?></span>
				<a href=""><svg class="social__icon"><use xlink:href="#ico-placemark"></use></svg><?= get_theme_mod('address', '')?></a>
				<a href="tel:+49<?=str_replace(array(' ', '(' , ')', '-'), '', get_theme_mod('phone_number', ''))?>"><svg class="social__icon"><use xlink:href="#ico-phone"></use></svg><?= get_theme_mod('phone_number', '')?></a>
			</div>
		</div>
		<div class="contacts__callback">
			<div class="widget__callback">
				<span>Sie haben Fragen oder benötigen weitere Informationen?</span>

				<?php echo do_shortcode( '[ninja_form id=3]' ); ?>
                <?php //echo do_shortcode( '[contact-form-7 id="20" title="Callback main contact"]' ); ?>
			</div>
		</div>
	</div>
</section>
