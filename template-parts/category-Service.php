<?php
/*
team, callback, blog & info blocks
*/
?><?php
$category = get_queried_object();
$term = get_category( $category->parent ) ? get_category( $category->parent ) : $category;
$params = [
        'term' => $term,
        'current' => $category
];
$title_page = '';
if(get_field('title_page', $category->taxonomy . '_' . $category->term_id)){
	$title_page = get_field('title_page', $category->taxonomy . '_' . $category->term_id);
}
elseif(get_field('title_page', $term->taxonomy . '_' . $term->term_id)){
	$title_page = get_field('title_page', $term->taxonomy . '_' . $term->term_id);
}
?>
	<main class="single-page">

		<!-- Section Top Banner -->
		<?php get_template_part( 'template-parts/section-page-banner', '', $params ); ?>
        <!-- .Section Top Banner -->

        <!-- Section Category Title -->
        <?php if($title_page) : ?>
		<section class="page__title">
			<div>
				<h2><?=$title_page;?><point>.</point></h2>
			</div>
		</section>
        <?php endif;?>
        <!-- .Section Category Title -->

        <!-- Section Category Post -->
        <?php if(have_posts()): ?>
		<section class="page__inner">
			<div class="container">
                <?php while ( have_posts() ) : the_post(); ?>
				<a class="page__card" href="<?php the_permalink();?>">
                    <div >
					<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' ); ?>
                    <?php if(is_array($large_image_url) && $large_image_url[0]) : ?>
                        <img src="<?=$large_image_url[0]?>">
                    <?php endif; ?>
                    <span class="card__title"><?php the_title();?></span>
                    <?php
					$value = get_field( "description_for_category_page" );
					if( $value ) {
						echo '<div class="card__caption">' . $value . '</div>';
					}
					?>
                </div>
                </a>
				<?php endwhile; ?>
            </div>
		</section>
		<?php endif; ?>
        <!-- .Section Category Post-->

        <!-- Section Category Slider -->
        <?php $slider_term = ''; if( have_rows('category_slider', $category) ) { $slider_term = $category;?>
        <?php } elseif( have_rows('category_slider', $term) ) { $slider_term = $term; } ?>
		<?php if( $slider_term ) :?>
        <section class="slider__procedure">
            <div class="container">
                <div class="slider__inner">
		    <?php	// loop through the rows of data
			while ( have_rows('category_slider', $slider_term) ) : the_row();
				$image = get_sub_field('slider_image');
			?>
                <div class="slider__item">
                    <div class="slider__img">
						<?php echo wp_get_attachment_image( $image['ID'], 'full' ); ?>
                    </div>
                    <div class="slider__content">
                        <div class="slider__inner__text">
							<?php the_sub_field('slider_caption'); ?>
                        </div>

                    </div>
                </div>
            <?php endwhile; ?>
                </div>
                <div class="slider__nav procedure__nav">
                    <div class="slider__count">
                        <span class="slider__count__current">1</span>/<span class="slider__count__total"></span>
                    </div>
                    <div class="slider__inner__nav"></div>
                </div>
            </div>
        </section>
        <?php endif; ?>
        <!-- .Section Category Slider -->

        <!-- Section Apparate -->
        <?php $image_apparate = $path_thumbnail = $path_large =[];
		for($i=1; $i < 5; ++$i){
		   if(get_field('image_apparate_' . $i, $category->taxonomy . '_' . $category->term_id)){
			   $image_apparate[$i] = get_field('image_apparate_' . $i, $category->taxonomy . '_' . $category->term_id);
			}
            elseif(get_field('image_apparate_' . $i, $term->taxonomy . '_' . $term->term_id)){
				$image_apparate[$i] = get_field('image_apparate_' . $i, $term->taxonomy . '_' . $term->term_id);
			}
			else{
				continue;
            }
			// (thumbnail, medium, large or custom size)
			$path_thumbnail[$i] = $image_apparate[$i]['sizes'][ 'medium' ];
			$path_large[$i] = $image_apparate[$i]['sizes'][ 'large' ];
		}
		if($path_thumbnail) : ?>
		<section class="apparate">
			<div class="container">
				<h2>Apparate, die verwenden.</h2>
				<div class="apparate__inner">

                    <?php foreach ($path_thumbnail as $key => $value) : if($value) { ?>
					<div class="apparate__img">
                        <a href="<?=$path_large[$key];?>" data-fancybox="images" data-caption="">
                            <img src="<?=$value;?>" alt="" />
                        </a>

					</div>
                    <?php } endforeach; ?>
				</div>
				<div class="apparate__background"></div>
			</div>
		</section>
        
        <?php endif; ?>
        <!-- Section Apparate -->

		<!-- Section Our Team -->
		<?php get_template_part( 'template-parts/section-our-team' ); ?>
		<!-- .Section Our Team -->

		<!-- Section Callback-block -->
		<?php get_template_part( 'template-parts/section-callback-block' ); ?>
		<!-- .Section Callback-block -->

		<!-- Section  Blog -->
		<?php get_template_part( 'template-parts/section-blog' ); ?>
		<!-- .Section Blog -->

		<!-- Section  Information -->
		<?php get_template_part( 'template-parts/section-information' ); ?>
		<!-- .Section Information -->

	</main>
