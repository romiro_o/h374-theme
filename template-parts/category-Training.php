<?php
/*
team, callback, blog & info blocks
*/
?><?php
$category = get_queried_object();
$title_page = '';
if(get_field('title_page', $category->taxonomy . '_' . $category->term_id)){
	$title_page = get_field('title_page', $category->taxonomy . '_' . $category->term_id);
}

?>
<main class="single-page training__template">
	<!-- Section Category Title -->
	<?php if($title_page) : ?>
		<section class="page__title">
			<div>
				<h2><?=$title_page;?><point>.</point></h2>
			</div>
		</section>
	<?php endif;?>
	<!-- .Section Category Title -->
    <!-- Section Shulungen -->
	<?php get_template_part( 'template-parts/section-shulungen' ); ?>
    <!-- .Section Shulungen -->

    <!-- Section Benefits -->
	<?php get_template_part( 'template-parts/section-benefits', '', ['term' => $category] ); ?>
	<!-- .Section Benefits -->


    <!-- Section More about Training -->
    <section class="more__training">
        <div class="container">

            <h2><?= get_field('title_block', $category->taxonomy . '_' . $category->term_id);?><point>.</point></h2>
            <div class="more__training__info">
                <div class="more__training__text">
					<?= get_field('content_block', $category->taxonomy . '_' . $category->term_id);?>
                </div>
				<?php if(have_rows('info_counter_about_training', $category->taxonomy . '_' . $category->term_id)){ ?>
                    <div class="more__training_data">
						<?php while ( have_rows('info_counter_about_training', $category->taxonomy . '_' . $category->term_id) ) : the_row();?>
                            <div class="training__data__item">
                                <span class="data__counter"><?= get_sub_field('data_counter');?></span>
                                <span class="description__counter"><?= get_sub_field('description_counter');?></span>
                            </div>
						<?php endwhile; ?>
                    </div>
                <?php } ?>
            </div>
			<?php if(have_rows('more_about_training_slider', $category->taxonomy . '_' . $category->term_id)){ ?>
            <div class="more__training__slider">
				<?php while ( have_rows('more_about_training_slider', $category->taxonomy . '_' . $category->term_id) ) : the_row();?>
                <div class="training__slider__image">
                    <img src="<?= get_sub_field('image')['sizes'][ 'large' ];?>">
                </div>
				<?php endwhile; ?>
            </div>

            <div class="training__nav slider__nav">
                <div class="slider__count">
                    <span class="slider__count__current">1</span>/<span class="slider__count__total"></span>
                </div>
                <div class="more__training__arrows"></div>
            </div>
			<?php } ?>
        </div>
        <div class="more__training__background"></div>
    </section>
    <!-- .Section More about Training -->

    <!-- Section Our Team -->
	<?php get_template_part( 'template-parts/section-our-team' ); ?>
    <!-- .Section Our Team -->

    <!-- Section Popular Course -->
	<?php get_template_part( 'template-parts/section-popular-course', '',
        ['term_id'=> $category->term_id, 'title_block' => 'Populäre Kurse' ] ); ?>
    <!-- .Section Popular Course -->

    <!-- Section  Blog -->
	<?php get_template_part( 'template-parts/section-blog' ); ?>
    <!-- .Section Blog -->

    <!-- Section  Information -->
	<?php get_template_part( 'template-parts/section-information' ); ?>
    <!-- .Section Information -->

</main>


