<?php
$children = get_terms(
	'category', [
		'parent' => 21, // ID Korperpflege  category
		'hide_empty' => false,
		'meta_key'			=> 'sort',
		'orderby'			=> 'meta_value',
		'order'				=> 'ASC',
		'posts_per_page' => -1
	]

);

?>
<?php if($children): ?>
<section id="Korperpflege" class="section__fluid second">
	<div class="section__inner">
		<div class="section__img">
			<img src="<?= get_theme_mod('image_korperpflege', '')?>">
		</div>
		<div class="section__info">
			<h2>Körperpflege<point style="color: #EBAC21;">.</point></h2>
			<ul>
				<?php foreach ($children as $sub_category) :?>
                    <li>
                        <a href="/category/<?=$sub_category->slug?>/"><?=$sub_category->name?></a>
                    </li>
				<?php endforeach;?>
				<?php wp_reset_postdata();?>
			</ul>
		</div>
	</div>
</section>
<?php endif;?>