<?php
$category_id = 6; //blog category
$args = array(
	'cat' => $category_id,
	'posts_per_page' => 4,
);
$blog = new WP_Query($args);
?>
<?php if($blog->have_posts()): ?>
<section id="Blog" class="blog">
	<div class="container">
		<h2>Blog<point style="color: #EBAC21;">.</point></h2>
		<div class="blog__inner load__post">
	<?php while($blog->have_posts()): $blog->the_post();?>
        <div class="item__blog">
				<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>

			<?php if(isset($large_image_url[0])) {?>
                <img src="<?=$large_image_url[0]?>">
			<?php } ?>
				<div class="blog__date"><span><?php the_date(); ?></span></div>
				<div class="blog__title"><a href="<?php the_permalink();?>"><h3><?php the_title(); ?></h3></a></div>
				<div class="blog__text"><span><?php the_excerpt(); ?></span><?php the_date(); ?></div>
			</div>
    <?php endwhile;?>
        </div>
		<?php
        $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		$max_pages = $blog->max_num_pages;
		
		if( $paged < $max_pages ) {
			echo '<div class="more__button" id="loadmore" >
		            <a href="#" data-taxonomy="post" data-category="' . $category_id . '" data-max_pages="'  . $max_pages . '" data-paged="' . $paged . '">Mehr erfahren</a>
	             </div>';
		}
		?>
    </div>
	<?php wp_reset_postdata();?>
</section>
<?php endif;?>