<?php
$children = get_terms(
	'category', [
		'parent' => 7, // ID Gesichtspflege  category
		'hide_empty' => false,
		'meta_key'			=> 'sort',
		'orderby'			=> 'meta_value',
		'order'				=> 'ASC',
        'posts_per_page' => -1
	]

);

?>
<?php if($children): ?>
<section id="Gesichtspflege" class="section__fluid first">
	<div class="section__inner">
		<div class="section__info">
			<h2>Gesichtspflege<point style="color: #EBAC21;">.</point></h2>
			<ul>
	        <?php foreach ($children as $sub_category) :?>
                <li>
                    <a href="/category/<?=$sub_category->slug?>/"><?=$sub_category->name?></a>
                </li>
            <?php endforeach;?>
		    <?php wp_reset_postdata();?>
			</ul>
		</div>
		<div class="section__img">
			<img src="<?= get_theme_mod('image_gesichtspflege', '')?>">
		</div>
	</div>
</section>
<?php endif;?>