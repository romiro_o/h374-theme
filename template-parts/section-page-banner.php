<?php
/*
* $args array
*/
$title = $label = $description = $class_price_from = '';
$widget_form = $form_callback = false;
$children = $image = [];
if(isset($args['term'])){ //var_dump($args['term']);
	$widget_form = true;
	if(isset($args['current']) ){
		$image = get_field('image', $args['current']->taxonomy . '_' . $args['current']->term_id);
		$title =$args['current']->name;
	}


	$image = !$image ? get_field('image', $args['term']->taxonomy . '_' . $args['term']->term_id) : $image;
	$size = 'large'; // (thumbnail, medium, large or custom size)
	$image_src = $image['sizes'][$size] ?? null;
	$children = get_terms(
		$args['term']->taxonomy, [
			'parent' => $args['term']->term_id,
			'hide_empty' => false,
			'meta_key'			=> 'sort',
			'orderby'			=> 'meta_value',
			'order'				=> 'ASC'
		]
	);
	$title = !$title ? $args['term']->name : $title;
	$description = $args['term']->description;
	$label = get_field('title_before_select_on_banner', $args['term']->taxonomy . '_' . $args['term']->term_id);
}
else{

	$widget_form = $form_callback = true;
	$title = get_the_title();
	$description = get_field('description_banner');
	$label = get_field('price_from');
	$class_price_from = 'price_from';
	if(get_field('banner_image')){
		$image_banner = get_field('banner_image');
		$image_src = $image_banner['sizes'][ 'large' ] ?? null;
    }
	else{
		$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		$image_src = $large_image_url[0] ?? null;
    }
}


?>
<section class="top__banner page__banner">
	<div class="container">
		<div class="top__banner__inner">
            <?php if($image_src){ ?>
			<div class="top__banner__img">
                <div>
                    <img src="<?= esc_url($image_src); ?>">
                </div>
            </div>
            <?php } ?>
			<div class="top__banner__text">
				<div>
					<?php if ( function_exists( 'breadcrumbs' ) ) breadcrumbs(); ?>
					<h1><?=$title; ?><point style="color: #EBAC21;">.</point></h1>
					<?php if($description){ ?>
                        <span><?=$description;?></span>
                    <?php }?>
                    <?php if($widget_form){ ?>
                    <div class="widget__callback">
						<?php if($label){ ?>
                            <span class="<?=$class_price_from;?>"><?=$label;?></span>
						<?php }?>
                        <?php if($children){ ?>
						<form class="form select_service">
							<!-- Уход за лицом -->
							<div class="form-item">
								<select >
                                    <?php foreach ($children as $sub_category) : ?>
									<option <?=($args['current']->slug == $sub_category->slug ? 'selected' : '');?> value="<?=get_category_link($sub_category->term_id)?>"><?=$sub_category->name?></option>
                                    <?php endforeach; ?>
								</select>
							</div>
                            <div class="form-item">
								<input id="select_service" type="submit" class="btn__light" value="Dienste ansehen">
							</div>
                        </form>
						<?php } elseif($form_callback){?>
                            <form class="form select_service">
                                <div class="form-item">
                                    <input id="select_service" type="submit" class="btn__light" value="Vereinbaren Sie einen Termin">
                                </div>
                            </form>
                        <?php } ?>
					</div>
                    <?php } ?>
                    <?php if(have_rows('sub_description_on_the_banner')){ ?>
                       <div class="sub__description">
						<?php	// loop through the rows of data
						while ( have_rows('sub_description_on_the_banner') ) : the_row();
							$image = get_sub_field('slider_image');
							?>
                           <div>
                               <span class="title__column"><?php the_sub_field('title_column'); ?></span>
                               <span class="description__column"><?php the_sub_field('description_column'); ?></span>
                           </div>
						<?php endwhile; ?>
                       </div>
                    <?php } ?>
                </div>

			</div>
		</div>

	</div>
</section>