<?php
$children = get_terms(
	'category', [
		'parent' => 5, // ID Behandlungen  category
		'posts_per_page' => -1,
	]
);
?>
<?php if($children): ?>
	<section id="Behandlungen" class="options">
		<div class="container">
			<div class="section__inner">
				<h2>Behandlungen<point style="color: #EBAC21;">.</point></h2>
				<ul>
					<?php $i = 0; foreach ($children as $sub_category) :
						if($i == 1){
							$image = get_field('image', 'category' . '_' . $sub_category->term_id);
							$size = 'large'; // (thumbnail, medium, large or custom size)
							$img_link = $image['sizes'][ $size ];
						}
						?>
						<li class="<?=($i == 1 ? 'active' : ''); ?>">
                            <a href="#" data-ajax-param="<?= $sub_category->term_id; ?>"><?= $sub_category->name;?></a>
							<div style="<?=($i == 1 ? 'display:block;' : ''); ?>">
                                <?php
								$args = array(
									'cat' => $sub_category->term_id, // ID Behandlungen  category
									'orderby' => 'ID',
									'order' => 'ASC',
									'posts_per_page' => -1,//all posts
								);
								$behandlungen = new WP_Query($args);
                                ?>
                                <ul>
									<?php while($behandlungen->have_posts()): $behandlungen->the_post(); ?>
                                        <li>
                                            <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                        </li>
										<?php endwhile;?>
									<?php wp_reset_postdata();?>
                                </ul>

                            </div>
						</li>
						<?php ++$i; endforeach;?>
					<?php wp_reset_postdata();?>
				</ul>
				<div class="section__img">
					<div id="post-img"><img src="<?=$img_link; ?>"></div>
				</div>
			</div>

		</div>
	</section>
<?php endif;?>



