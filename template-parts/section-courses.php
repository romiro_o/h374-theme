<section id="Shulungen" class="service">
	<div class="container">
		<h2>Schulungen<point style="color: #EBAC21;">.</point></h2>
		<div class="service_inner">
			<div class="service__block">
				<img src="<?=get_template_directory_uri();?>/images/service-1.jpg">
				<div><span>Haarentfernung Laser Schulung</span></div>
			</div>
			<div class="service__block">
				<img src="<?=get_template_directory_uri();?>/images/service-2.jpg">
				<div><span>Cryolipolyse Schulung</span></div>
			</div>
			<div class="service__block">
				<img src="<?=get_template_directory_uri();?>/images/service-3.jpg">
				<div><span>Haarentfernung SHR Schulung</span></div>
			</div>
			<div class="service__block">
				<img src="<?=get_template_directory_uri();?>/images/service-4.jpg">
				<div><span>IPL therapie Schulung</span></div>
			</div>
		</div>
		<div class="more__button"><a href="#">Alles sehen</a></div>
	</div>
</section>