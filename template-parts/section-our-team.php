<?php
$category_id = 4; //team category
$param = array(
	'cat' => $category_id, // ID of a page, post, or custom type
	'posts_per_page' => -1,
	'post_status' => 'publish'
);
$team = new WP_Query($param);
$selected = isset($args['selected']) ? 'selected' : '';
?>
<?php if($team->have_posts()): ?>
	<section class="team">
		<div class="container">
			<h2>Unser Team.</h2>
			<div class="team__inner">
				<?php $i = 1; while($team->have_posts()): $team->the_post();?>
					<div class="team__item <?=($i == 2 ? $selected : '');?>" data-member-id="<?= get_the_ID(); ?>">
						<a href="<?=get_permalink();?>">
							<div class="team__img">
								<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
								<img data-lazy="<?=$large_image_url[0]?>">
							</div>
						</a>
						<div class="team__info">
							<h3><a href="<?=get_permalink();?>"><?=the_title();?></a></h3>
							<span><?=get_field('job_title', get_the_ID()); ?></span>
						</div>
					</div>
				<?php ++$i; endwhile;?>
				<?php wp_reset_postdata();?>
			</div>
			<div class="slider__nav">
				<div class="slider__count">
					<span class="slider__count__current">1</span>/<span class="slider__count__total"></span>
				</div>
				<div class="slider__arrows"></div>
			</div>

		</div>
        <div class="team__background"></div>
	</section>
<?php endif;?>
