<?php
/*

Template Name: Training course 
Template post type: post, page

*/
get_header();
?>
<?php
$category = get_queried_object();
$term = get_category( $category->parent );
$params = [
	'term' => $term,
	'current' => $category
];
?>
    <main class="single-page">
        <!-- Section Top Banner -->
		<?php get_template_part( 'template-parts/section-post-banner', '', ['button_title' => 'Für einen Kurs anmelden']); ?>
        <!-- .Section Top Banner -->
        <!-- Section Category Title -->
        <section class="page__title">
            <div>
                <h2><?php the_title();?><point>.</point></h2>
            </div>
        </section>
        <!-- .Section Category Title -->

        <!-- Section Post Content-->
        <section>
            <div class="container">
				<?php the_content(); ?>
            </div>
        </section>
        <!-- .Section Post Content -->

        <!-- Section Course Tabs -->
        <section>
                <div class="container">
                    <div class="tabset">
                        <!-- Tab 1 -->
                        <input type="radio" name="tab_text" id="tab_title_first_tab" aria-controls="title_first_tab" checked="checked">
                        <label for="tab_title_first_tab"><?= get_field('title_first_tab');?></label>
                                <!-- Tab 2 -->
                        <input type="radio" name="tab_text" id="tab_title_second_tab" aria-controls="title_second_tab">
                        <label for="tab_title_second_tab"><?= get_field('title_second_tab');?></label>
                                <!-- Tab 3 -->
                        <input type="radio" name="tab_text" id="tab_title_third_tab" aria-controls="title_third_tab">
                        <label for="tab_title_third_tab"><?= get_field('title_third_tab');?></label>
                                <!-- Tab 4 -->
                        <input type="radio" name="tab_text" id="tab_title_forth_tab" aria-controls="title_forth_tab">
                        <label for="tab_title_forth_tab"><?= get_field('title_forth_tab');?></label>
                        <div class="tab-panels">
                            <section id="title_first_tab" class="tab-panel"><?= get_field('description_of_course');?></section>
                            <section id="title_second_tab" class="tab-panel">
	                            <?php if(have_rows('program_of_course')){ ?>
                                    <?php	// loop through the rows of data
									while ( have_rows('program_of_course') ) : the_row(); ?>
                                    <div class="program__row">
                                        <div class="program__column">
                                            <span class="program__day"><?= get_sub_field('number_day'); ?></span>
                                        </div>
                                        <div class="program__column">
                                            <div class="program__title"><?= get_sub_field('title'); ?></div>
                                            <div class="program__content"><?= get_sub_field('content'); ?></div>
                                        </div>
                                    </div>

                                    <?php endwhile; ?>
								<?php } ?>
                            </section>
                            <section id="title_third_tab" class="tab-panel">
                                <?php if(have_rows('schedule_of_course')){ ?>
									<?php	// loop through the rows of data
									while ( have_rows('schedule_of_course') ) : the_row(); ?>
                                        <div class="schedule__row">
                                            <span><?= get_sub_field('period'); ?></span>
                                            <span><?= get_sub_field('time'); ?></span>
                                            <span><?= get_sub_field('places_available'); ?></span>
                                        </div>
                                    <?php endwhile; ?>
								<?php } ?>
                                <div class="group__buttons">
                                    <a href="#" class="request__link" data-title="Alle Gruppen">Alle Gruppen</a>
                                    <a href="#contact_form_pop" class="request__link fancybox-inline btn__light" data-title="Für einen Kurs anmelden">Für einen Kurs anmelden</a>
                                </div>

                            </section>
                            <section id="title_forth_tab" class="tab-panel">
                                <div class="diplomas">
									<?php
									$images = get_field('diplomas');
									if( $images ): ?>
										<?php foreach( $images as $image ): ?>
                                            <div>
                                                <a href="<?php echo $image['url']; ?>" data-fancybox="images" data-caption="">
                                                    <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                </a>
                                                <p><?php echo $image['caption']; ?></p>
                                            </div>
										<?php endforeach; ?>
									<?php endif; ?>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
        <!-- .Section Course Tabs -->

        <!-- Section Benefits -->
		<?php get_template_part( 'template-parts/section-benefits'); ?>
        <!-- .Section Benefits -->

        <!-- Section Post Tabs Gallery -->
		<?php get_template_part( 'template-parts/section-tabs-gallery', '', $params); ?>
        <!-- .Section Post Tabs -->

        <!-- Section Popular Course -->
		<?php get_template_part( 'template-parts/section-popular-course'); ?>
        <!-- .Section Popular Course -->

        <!-- Section  Blog -->
		<?php get_template_part( 'template-parts/section-blog' ); ?>
        <!-- .Section Blog -->

        <!-- Section  Information -->
		<?php get_template_part( 'template-parts/section-information' ); ?>
        <!-- .Section Information -->


    </main>
<?php setPostViews(get_the_ID()); ?>
<?php
get_footer();
