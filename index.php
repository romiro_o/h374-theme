<?php get_header();?>

	<!-- Section Top Banner -->
	<?php get_template_part( 'template-parts/section-top-banner' ); ?>
	<!-- .Section Top Banner -->

	<!-- Section Top callback -->
	<?php get_template_part( 'template-parts/section-top-callback' ); ?>
	<!-- .Section Top callback -->

    <?php if(!get_theme_mod('hide_gesichtspflege', '')){ ?>
	<!-- Section Gesichtspflege -->
	<?php get_template_part( 'template-parts/section-gesichtspflege' ); ?>
	<!-- .Section Gesichtspflege -->
    <?php } ?>

    <?php if(!get_theme_mod('hide_korperpflege', '')){ ?>
	<!-- Section Korperpflege -->
	<?php get_template_part( 'template-parts/section-korperpflege' ); ?>
	<!-- .Section Korperpflege -->
    <?php } ?>

    <?php if(!get_theme_mod('hide_behandlungen', '')){ ?>
	<!-- Section Behandlungen -->
	<?php get_template_part( 'template-parts/section-behandlungen' ); ?>
	<!-- .Section Behandlungen -->
    <?php } ?>

    <?php if(!get_theme_mod('hide_fur_sie', '')){ ?>
	<!-- Section Fur Sie -->
	<?php get_template_part( 'template-parts/section-fur-sie' ); ?>
	<!-- .Section Fur Sie -->
    <?php } ?>

    <?php if(!get_theme_mod('hide_callback_block', '')){ ?>
	<!-- Section Callback-block -->
	<?php get_template_part( 'template-parts/section-callback-block' ); ?>
	<!-- .Section Callback-block -->
    <?php } ?>

    <?php if(!get_theme_mod('hide_schulungen', '')){ ?>
	<!-- Section Shulungen -->
	<?php get_template_part( 'template-parts/section-shulungen', '', ['term_id'=> 29]); ?>
	<!-- .Section Shulungen -->
    <?php } ?>

    <?php if(!get_theme_mod('hide_unser_team', '')){ ?>
    <!-- Section Our Team -->
	<?php get_template_part( 'template-parts/section-our-team' ); ?>
	<!-- .Section Our Team -->
    <?php } ?>

    <?php if(!get_theme_mod('hide_partner', '')){ ?>
	<!-- Section  Partners -->
	<?php get_template_part( 'template-parts/section-partners' ); ?>
	<!-- .Section Partners -->
    <?php } ?>

    <?php if(!get_theme_mod('hide_blog', '')){ ?>
	<!-- Section  Blog -->
	<?php get_template_part( 'template-parts/section-blog' ); ?>
	<!-- .Section Blog -->
    <?php } ?>

    <?php if(!get_theme_mod('hide_contact', '')){ ?>
	<!-- Section  Information -->
	<?php get_template_part( 'template-parts/section-information' ); ?>
	<!-- .Section Information -->
    <?php } ?>

<?php get_footer();?>